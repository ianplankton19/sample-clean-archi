package com.whitecloak.perahub.data.webservice.client

import com.whitecloak.perahub.data.helper.ResponseHelper
import com.whitecloak.perahub.data.webservice.PeraHubService
import com.whitecloak.perahub.domain.models.User
import com.whitecloak.perahub.domain.repositories.params.LoginParam
import com.whitecloak.perahub.domain.repositories.params.ValidateOtpParam
import com.whitecloak.perahub.domain.repositories.resp.OtpResp
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by ralph on 15/03/2018.
 */

class UserClient @Inject constructor(private val peraHubService: PeraHubService,
                                     private val responseHelper: ResponseHelper) {

    fun login(param: LoginParam): Observable<OtpResp> {
        return peraHubService.login(param)
            .compose(responseHelper.handleResp())
    }

    fun verifyLogin(param: ValidateOtpParam): Observable<User> {
        return peraHubService.verifyLogin(param)
                .compose(responseHelper.handleResp())
    }

}