package com.whitecloak.perahub.data.webservice.response

import com.google.gson.annotations.SerializedName

/**
 * Created by ralph on 16/03/2018.
 */

data class ServiceErrorResp(@SerializedName("errors") val errors: List<ServiceError>) {

    fun message(): String {
        var message = ""

        for (error in errors) {
            message += error.message
            if (errors.indexOf(error) < errors.size - 1) {
                message += "\n"
            }
        }

        if (message.isEmpty()) {
            message = "Something went wrong."
        }

        return message
    }
}