package com.whitecloak.perahub.data.webservice

import com.whitecloak.perahub.domain.models.User
import com.whitecloak.perahub.domain.repositories.params.*
import com.whitecloak.perahub.domain.repositories.resp.OtpResp
import com.whitecloak.perahub.domain.repositories.resp.RequestRegisterOtpResp
import com.whitecloak.perahub.domain.repositories.resp.ValidateRegisterOtpResp
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

/**
 * Created by johneris on 2/28/18.
 */

interface PeraHubService {

    companion object {
        val HEADER_AUTHORIZATION = "X-Authorization"
    }

    @POST("login")
    fun login(@Body param: LoginParam): Observable<Response<OtpResp>>

    @POST("verify-login")
    fun verifyLogin(@Body param: ValidateOtpParam): Observable<Response<User>>


    @POST("otp/request-register")
    fun requestRegisterOtp(@Body param: RequestRegisterOtpParam): Observable<Response<RequestRegisterOtpResp>>

    @POST("otp/verify-register")
    fun validateRegisterOtp(@Body param: ValidateRegisterOtpParam): Observable<Response<ValidateRegisterOtpResp>>

    @POST("register")
    fun register(@Body param: RegisterParam): Observable<Response<User>>

}