package com.whitecloak.perahub.data.webservice.response

import com.google.gson.annotations.SerializedName

/**
 * Created by johneris on 2/28/18.
 */

data class DataResp<out T>(
        @SerializedName("user") val data: T
)