package com.whitecloak.perahub.data.repositories

import android.content.SharedPreferences
import com.google.gson.Gson
import com.whitecloak.perahub.data.exception.RegistrationTokenNotSetException
import com.whitecloak.perahub.data.exception.UserNotLoggedInException
import com.whitecloak.perahub.data.webservice.client.UserClient
import com.whitecloak.perahub.domain.models.User
import com.whitecloak.perahub.domain.repositories.UserRepository
import com.whitecloak.perahub.domain.repositories.params.LoginParam
import com.whitecloak.perahub.domain.repositories.params.ValidateOtpParam
import com.whitecloak.perahub.domain.repositories.resp.OtpResp
import io.reactivex.Completable
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by johneris on 2/27/18.
 */

@Singleton
class UserDataRepository @Inject
constructor(private val gson: Gson,
            private val sharedPreferences: SharedPreferences,
            private val userClient: UserClient) : UserRepository {

    private val PREFS_USER = "PREFS_USER"

    private var loginOtpToken: String? = null

    override fun getLoginOtpToken(): Observable<String> {
        return Observable.fromCallable {
            if (loginOtpToken == null) {
                throw RegistrationTokenNotSetException()
            }
            return@fromCallable loginOtpToken
        }
    }

    override var loggedInUser: Observable<User>
        get() {
            return Observable.fromCallable {
                // change to encrypted
                val userJson = sharedPreferences.getString(PREFS_USER, "")
                if (userJson.isEmpty()) {
                    throw UserNotLoggedInException()
                }
                val user = gson.fromJson(userJson, User::class.java)
                if (user == null) {
                    throw UserNotLoggedInException()
                }
                return@fromCallable user
            }
        }
        set(value) {
            value.map {
                val json = gson.toJson(it)
                sharedPreferences.edit().putString(PREFS_USER, json).commit()
            }
        }

    override fun login(param: LoginParam): Observable<OtpResp> {
        return userClient.login(param)
    }

    override fun saveLoginData(user: User) {
        val userTemp: User = user
        val json = gson.toJson(userTemp)
        sharedPreferences.edit().putString(PREFS_USER, json).commit()

    }

    override fun saveLoginOtpToken(token: String): Completable {
        return Completable.fromAction {
            this.loginOtpToken = token
        }
    }

    override fun validateLogin(param: ValidateOtpParam): Observable<User> {
        return userClient.verifyLogin(param)
    }

}
