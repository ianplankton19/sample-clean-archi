package com.whitecloak.perahub.data.helper

import com.google.gson.Gson
import com.whitecloak.perahub.data.webservice.operator.ServiceOperator
import io.reactivex.ObservableTransformer
import retrofit2.Response
import javax.inject.Inject

/**
 * Created by ralph on 16/03/2018.
 */

class ResponseHelper @Inject constructor(private val gson: Gson) {

    fun <T> handleResp(): ObservableTransformer<Response<T>, T> {
        return ObservableTransformer { upstream ->
            upstream.lift(ServiceOperator(gson))
        }
    }
}