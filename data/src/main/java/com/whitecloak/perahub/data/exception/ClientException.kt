package com.whitecloak.perahub.data.exception

import java.lang.Exception

/**
 * Created by ralph on 16/03/2018.
 */

class ClientException : Exception {
    constructor(message: String?, cause: Throwable?) : super(message, cause)
}