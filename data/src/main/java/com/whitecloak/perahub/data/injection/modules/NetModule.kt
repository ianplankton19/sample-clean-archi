package com.whitecloak.perahub.data.injection.modules

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.whitecloak.perahub.data.helper.ResponseHelper
import com.whitecloak.perahub.data.webservice.PeraHubService
import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton

/**
 * Created by johneris on 2/28/18.
 */

@Module
class NetModule {

    @Singleton
    @Provides
    fun provideGson(): Gson {
        val gson = GsonBuilder()
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .create()
        return gson
    }

    @Singleton
    @Provides
    fun provideResponseHelper(gson: Gson) : ResponseHelper {
        return ResponseHelper(gson)
    }

    @Singleton
    @Provides
    fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        return loggingInterceptor
    }

    @Singleton
    @Provides
    fun provideOkHttpClient(loggingInterceptor: HttpLoggingInterceptor): OkHttpClient {
        val httpClient = OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .build()
        return httpClient
    }

    @Singleton
    @Provides
    fun provideRetrofit(@Named("baseUrl") baseUrl: String, gson: Gson, okHttpClient: OkHttpClient): Retrofit {
        val retrofit = Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .build()
        return retrofit
    }

    @Singleton
    @Provides
    fun providePeraHubService(retrofit: Retrofit): PeraHubService {
        return retrofit.create(PeraHubService::class.java)
    }

}