package com.whitecloak.perahub.data.exception

/**
 * Created by johneris on 3/16/18.
 */

class RegistrationTokenNotSetException : Exception() {

    override fun getLocalizedMessage(): String {
        return "Registration token not set."
    }

}