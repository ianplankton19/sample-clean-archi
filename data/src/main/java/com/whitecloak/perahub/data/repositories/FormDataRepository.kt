package com.whitecloak.perahub.data.repositories

import com.whitecloak.perahub.domain.form.model.RegistrationForm
import com.whitecloak.perahub.domain.form.repository.FormRepository
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by ralph on 14/03/2018.
 */

@Singleton
class FormDataRepository @Inject constructor() : FormRepository {

    private var registrationForm: RegistrationForm? = null

    override fun setRegistrationForm(form: RegistrationForm?) {
        registrationForm = form;
    }

    override val getRegistrationForm: RegistrationForm?
        get() = registrationForm
}