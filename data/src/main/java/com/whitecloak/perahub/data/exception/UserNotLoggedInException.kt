package com.whitecloak.perahub.data.exception

/**
 * Created by johneris on 3/7/18.
 */

class UserNotLoggedInException : Exception() {

    override fun getLocalizedMessage(): String {
        return "User not logged in."
    }

}
