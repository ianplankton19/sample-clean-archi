package com.whitecloak.perahub.data.webservice.operator

import com.google.gson.Gson
import com.whitecloak.perahub.data.exception.ClientException
import com.whitecloak.perahub.data.exception.NoNetworkException
import com.whitecloak.perahub.data.exception.ServerException
import com.whitecloak.perahub.data.exception.ServiceException
import com.whitecloak.perahub.data.webservice.response.ServiceErrorResp
import io.reactivex.ObservableOperator
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import io.reactivex.exceptions.CompositeException
import retrofit2.Response
import java.net.SocketException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

/**
 * Created by ralph on 16/03/2018.
 */

class ServiceOperator<T> constructor(private val gson: Gson) : ObservableOperator<T, Response<T>> {

    override fun apply(observer: Observer<in T>): Observer<in Response<T>> {
        return object : Observer<Response<T>> {
            override fun onComplete() {
                observer.onComplete()
            }

            override fun onSubscribe(d: Disposable) {
                observer.onSubscribe(d)
            }

            override fun onNext(response: Response<T>) {
                val code = response.code()
                try {
                    if (code == 400 || code == 401 || code == 403 || code == 404 || code == 422) {
                        val jsonError = response.errorBody()?.string()
                        val errorResp: ServiceErrorResp =
                                gson.fromJson(jsonError, ServiceErrorResp::class.java)

                        observer.onError(ServiceException(errorResp.message()))

                        return
                    }

                    response.body()?.let { observer.onNext(it) }
                } catch (e: Exception) {
                    val message = "Sorry, something went wrong. " +
                            "We’re working on it and we’ll get it fixed as soon as we can. Ref: S3"
                    observer.onError(ServerException(message, e))
                }
            }

            override fun onError(e: Throwable) {
                if (e is SocketException
                        || e is SocketTimeoutException
                        || e is UnknownHostException
                        || e is NoNetworkException) {
                    val message = "Uh oh! Looks like you lost your internet connection. " +
                            "Please check your network settings and try again later."
                    observer.onError(ClientException(message, e))
                }
            }
        }
    }
}