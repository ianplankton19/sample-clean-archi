package com.whitecloak.perahub.data.exception

/**
 * Created by ralph on 16/03/2018.
 */

class NoNetworkException : RuntimeException {
    constructor(
        message: String?,
        cause: Throwable?,
        enableSuppression: Boolean,
        writableStackTrace: Boolean
    ) : super(message, cause, enableSuppression, writableStackTrace)
}