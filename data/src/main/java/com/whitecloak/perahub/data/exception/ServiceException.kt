package com.whitecloak.perahub.data.exception

import java.lang.Exception

/**
 * Created by ralph on 16/03/2018.
 */

class ServiceException : Exception {
    constructor(message: String?) : super(message)
}