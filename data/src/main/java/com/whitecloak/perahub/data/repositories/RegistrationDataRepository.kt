package com.whitecloak.perahub.data.repositories

import com.google.gson.Gson
import com.whitecloak.perahub.data.exception.RegistrationTokenNotSetException
import com.whitecloak.perahub.data.webservice.client.RegistrationClient
import com.whitecloak.perahub.domain.models.User
import com.whitecloak.perahub.domain.repositories.RegistrationRepository
import com.whitecloak.perahub.domain.repositories.params.RegisterParam
import com.whitecloak.perahub.domain.repositories.params.RequestRegisterOtpParam
import com.whitecloak.perahub.domain.repositories.params.ValidateRegisterOtpParam
import com.whitecloak.perahub.domain.repositories.resp.RequestRegisterOtpResp
import com.whitecloak.perahub.domain.repositories.resp.ValidateRegisterOtpResp
import io.reactivex.Completable
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by johneris on 3/14/18.
 */

@Singleton
class RegistrationDataRepository @Inject
constructor(private val gson: Gson,
            private val registrationClient: RegistrationClient) : RegistrationRepository {

    private var otpToken: String? = null
    private var registrationToken: String? = null

    override fun getOtpToken(): Observable<String> {
        return Observable.fromCallable {
            if (otpToken == null) {
                throw RegistrationTokenNotSetException()
            }
            return@fromCallable otpToken
        }
    }

    override fun getRegistrationToken(): Observable<String> {
        return Observable.fromCallable {
            if (registrationToken == null) {
                throw RegistrationTokenNotSetException()
            }
            return@fromCallable registrationToken
        }
    }

    override fun register(param: RegisterParam): Observable<User> {
        return registrationClient.register(param)
    }

    override fun requestRegisterOtp(param: RequestRegisterOtpParam): Observable<RequestRegisterOtpResp> {
        return registrationClient.requestRegisterOtp(param)
    }

    override fun saveRegistrationToken(token: String): Completable {
        return Completable.fromAction {
            this.registrationToken = token
        }
    }

    override fun saveOtpToken(token: String): Completable {
        return Completable.fromAction {
            this.otpToken = token
        }
    }

    override fun validateRegisterOtp(param: ValidateRegisterOtpParam): Observable<ValidateRegisterOtpResp> {
        return registrationClient.validateRegisterOtp(param)
    }

}