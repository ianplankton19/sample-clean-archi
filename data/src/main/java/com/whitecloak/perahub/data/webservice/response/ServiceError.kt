package com.whitecloak.perahub.data.webservice.response

/**
 * Created by ralph on 16/03/2018.
 */

data class ServiceError(
    val status: String,
    val code: String,
    val message: String
)