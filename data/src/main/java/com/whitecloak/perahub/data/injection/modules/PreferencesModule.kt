package com.whitecloak.perahub.data.injection.modules

import android.content.Context
import android.content.SharedPreferences
import com.securepreferences.SecurePreferences
import dagger.Module
import dagger.Provides
import javax.inject.Named
import javax.inject.Singleton

/**
 * Created by johneris on 2/28/18.
 */

@Module
class PreferencesModule {

    @Singleton
    @Provides
    fun provideSharedPreferences(@Named("packageName") packageName: String,
                                 context: Context): SharedPreferences {
        return context.getSharedPreferences(packageName, Context.MODE_PRIVATE)
    }

    @Singleton
    @Provides
    fun provideSecurePreferences(@Named("packageName") packageName: String,
                                 context: Context): SecurePreferences {
        return SecurePreferences(context, "", packageName + "_secure")
    }

}