package com.whitecloak.perahub.data.webservice.client

import com.whitecloak.perahub.data.helper.ResponseHelper
import com.whitecloak.perahub.data.webservice.PeraHubService
import com.whitecloak.perahub.domain.models.User
import com.whitecloak.perahub.domain.repositories.params.RegisterParam
import com.whitecloak.perahub.domain.repositories.params.RequestRegisterOtpParam
import com.whitecloak.perahub.domain.repositories.params.ValidateRegisterOtpParam
import com.whitecloak.perahub.domain.repositories.resp.RequestRegisterOtpResp
import com.whitecloak.perahub.domain.repositories.resp.ValidateRegisterOtpResp
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by johneris on 3/16/18.
 */

class RegistrationClient @Inject constructor(private val peraHubService: PeraHubService,
                                             private val responseHelper: ResponseHelper) {

    fun requestRegisterOtp(param: RequestRegisterOtpParam): Observable<RequestRegisterOtpResp> {
        return peraHubService.requestRegisterOtp(param)
                .compose(responseHelper.handleResp())
    }

    fun validateRegisterOtp(param: ValidateRegisterOtpParam): Observable<ValidateRegisterOtpResp> {
        return peraHubService.validateRegisterOtp(param)
                .compose(responseHelper.handleResp())
    }

    fun register(param: RegisterParam): Observable<User> {
        return peraHubService.register(param)
                .compose(responseHelper.handleResp())
    }

}