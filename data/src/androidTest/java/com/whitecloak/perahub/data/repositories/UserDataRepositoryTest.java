package com.whitecloak.perahub.data.repositories;

import android.app.Application;
import android.content.Context;
import android.support.test.InstrumentationRegistry;

import com.google.gson.Gson;
import com.securepreferences.SecurePreferences;
import com.whitecloak.perahub.data.exception.UserNotLoggedInException;
import com.whitecloak.perahub.data.helpers.DaggerHelper;
import com.whitecloak.perahub.data.helpers.SampleJsonHelper;
import com.whitecloak.perahub.domain.models.User;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import io.reactivex.observers.TestObserver;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by johneris on 3/6/18.
 */

@RunWith(MockitoJUnitRunner.class)
public class UserDataRepositoryTest {

    private Context context;
    private Application application;

    @Mock
    Gson gson;

    @Mock
    SecurePreferences securePreferences;

    @Mock
    User user;

    @Before
    public void setUp() {
        application = (Application) InstrumentationRegistry.getTargetContext().getApplicationContext();
        context = InstrumentationRegistry.getContext();
        DaggerHelper.getTestAppComponent(application, context).inject(this);
    }

    @Test
    public void createMock() throws Exception {
        Gson gson = mock(Gson.class);
        SecurePreferences securePreferences = mock(SecurePreferences.class);

        new UserDataRepository(gson, securePreferences);
    }

    @Test
    public void testGetLoggedInUser_shouldReturnUser_whenHasSavedUser() throws Exception {
        String userJson = SampleJsonHelper.getUser(context);
        User expectedUser = user;

        when(securePreferences.getString(anyString(), anyString())).thenReturn(userJson);
        when(gson.fromJson(anyString(), any())).thenReturn(expectedUser);

        UserDataRepository userDataRepository = new UserDataRepository(gson, securePreferences);

        TestObserver<User> testObserver = userDataRepository.getLoggedInUser().test();
        testObserver.assertResult(expectedUser);
    }

    @Test
    public void testGetLoggedInUser_shouldError_whenPrefsIsEmpty() throws Exception {
        when(securePreferences.getString(anyString(), anyString())).thenReturn("");

        UserDataRepository userDataRepository = new UserDataRepository(gson, securePreferences);

        TestObserver<User> testObserver = userDataRepository.getLoggedInUser().test();
        testObserver.assertError(UserNotLoggedInException.class);
    }

    @Test
    public void testGetLoggedInUser_shouldError_whenGsonCantParse() throws Exception {
        String userJson = SampleJsonHelper.getUser(context);
        User expectedUser = null;

        when(securePreferences.getString(anyString(), anyString())).thenReturn(userJson);
        when(gson.fromJson(anyString(), any())).thenReturn(expectedUser);

        UserDataRepository userDataRepository = new UserDataRepository(gson, securePreferences);

        TestObserver<User> testObserver = userDataRepository.getLoggedInUser().test();
        testObserver.assertError(UserNotLoggedInException.class);
    }

}