package com.whitecloak.perahub.data.helpers;

import android.content.Context;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by johneris on 3/6/18.
 */

public class SampleJsonHelper {

    private static final String JSON_RESPONSE_BASE_FOLDER = "json";

    public static String getUser(Context context) throws Exception {
        String fileName = JSON_RESPONSE_BASE_FOLDER + "/user.json";
        return getStringFromFile(context, fileName);
    }



    private static String convertStreamToString(InputStream is) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            sb.append(line).append("\n");
        }
        reader.close();
        return sb.toString();
    }

    private static String getStringFromFile(Context context, String filePath) throws Exception {
        final InputStream stream = context.getResources().getAssets().open(filePath);

        String ret = convertStreamToString(stream);
        //Make sure you close all streams.
        stream.close();
        return ret;
    }

}
