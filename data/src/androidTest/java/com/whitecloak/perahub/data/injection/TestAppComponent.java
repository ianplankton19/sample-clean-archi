package com.whitecloak.perahub.data.injection;


import android.app.Application;
import android.content.Context;

import com.whitecloak.perahub.data.deserialization.UserDeserializationTest;
import com.whitecloak.perahub.data.injection.modules.NetModule;
import com.whitecloak.perahub.data.injection.modules.PreferencesModule;
import com.whitecloak.perahub.data.repositories.UserDataRepositoryTest;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;

/**
 * Created by johneris on 2/28/18.
 */

@Singleton
@Component(modules = {
        NetModule.class,
        PreferencesModule.class
})
public interface TestAppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);

        @BindsInstance
        Builder context(Context context);

        @BindsInstance
        Builder baseUrl(@Named("baseUrl") String baseUrl);

        @BindsInstance
        Builder packageName(@Named("packageName") String packageName);

        TestAppComponent build();
    }


    // Repository

    void inject(UserDataRepositoryTest userDataRepositoryTest);

    // Deserialization

    void inject(UserDeserializationTest userDeserializationTest);

}
