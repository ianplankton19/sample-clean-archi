package com.whitecloak.perahub.data.helpers;

import android.app.Application;
import android.content.Context;

import com.whitecloak.perahub.data.injection.DaggerTestAppComponent;
import com.whitecloak.perahub.data.injection.TestAppComponent;

/**
 * Created by johneris on 3/6/18.
 */

public class DaggerHelper {

    public static TestAppComponent getTestAppComponent(Application application, Context context) {
        String prefsPackageName = "com.whitecloak.perahub.user";
        String baseUrl = "https://shanti-dope.com";

        return DaggerTestAppComponent.builder()
                .application(application)
                .context(context)
                .baseUrl(baseUrl)
                .packageName(prefsPackageName)
                .build();
    }

}
