package com.whitecloak.perahub.data.deserialization;

import android.app.Application;
import android.content.Context;
import android.support.test.InstrumentationRegistry;

import com.google.gson.Gson;
import com.whitecloak.perahub.data.helpers.DaggerHelper;
import com.whitecloak.perahub.data.helpers.SampleJsonHelper;
import com.whitecloak.perahub.domain.models.User;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import javax.inject.Inject;

import static junit.framework.Assert.assertEquals;

/**
 * Created by johneris on 3/13/18.
 */

@RunWith(MockitoJUnitRunner.class)
public class UserDeserializationTest {

    private Context context;
    private Application application;

    @Inject
    Gson gson;

    @Before
    public void setUp() {
        application = (Application) InstrumentationRegistry.getTargetContext().getApplicationContext();
        context = InstrumentationRegistry.getContext();
        DaggerHelper.getTestAppComponent(application, context).inject(this);
    }

    @Test
    public void testUserDeserialization() throws Exception {
        String userJson = SampleJsonHelper.getUser(context);

        User user = gson.fromJson(userJson, User.class);

        assertEquals(user.getUsername(), "wc_ian_blank_6");
        assertEquals(user.getMobile(), "09425475749");
        assertEquals(user.getEmail(), "ian.blanko@whitecloak.com");
        assertEquals(user.getPhone(), "09425475749");
        assertEquals(user.getSurname(), "blanko");
        assertEquals(user.getFirstName(), "Ian");
        assertEquals(user.getMiddleName(), "java Lord");
        assertEquals(user.getPassword(), "wc_pass");
        assertEquals(user.getBirthdate(), "12/25/1975");
        assertEquals(user.getNationality(), "Angola");
        assertEquals(user.getOccupation(), "12312312");
        assertEquals(user.getNameOfEmployer(), "White Cloak");
        assertEquals(user.getSecretQuestion1(), "hello");
        assertEquals(user.getAnswer1(), "world");
        assertEquals(user.getSecretQuestion2(), "hello");
        assertEquals(user.getAnswer2(), "world");
        assertEquals(user.getSecretQuestion3(), "hello");
        assertEquals(user.getAnswer3(), "world");
        assertEquals(user.getValidIdentification(), "015");
        assertEquals(user.getIdImage(), "img/blank.png");
        assertEquals(user.getWuCardNo(), "");
        assertEquals(user.getDebitCardNo(), "");
        assertEquals(user.getLoyaltyCardNo(), "");
        assertEquals(user.getValidIdNumber(), 1231312);
        assertEquals(user.getIdCountryIssue(), "Aland Islands");
        assertEquals(user.getIdIssueData(), "01/01/1700");
        assertEquals(user.getGender(), "N");
        assertEquals(user.isWalkin(), "1");
        assertEquals(user.getCustomerId(), "123456789");
        assertEquals(user.getIdType(), "O");
        assertEquals(user.getIdExpirationDate(), "10/05/2017");
        assertEquals(user.getCountryOfBirth(), "Antarctica");
        assertEquals(user.getTin(), "none");
        assertEquals(user.getSss(), 13);

        assertEquals(user.getPresentAddress().getAddress(), "test");
        assertEquals(user.getPresentAddress().getCity(), "San Mateo");
        assertEquals(user.getPresentAddress().getState(), "Rizal");
        assertEquals(user.getPresentAddress().getProvince(), "test");
        assertEquals(user.getPresentAddress().getRegion(), "IV-A");
        assertEquals(user.getPresentAddress().getCountry(), "Phillipines");
        assertEquals(user.getPresentAddress().getPostalCode(), "1990");

        assertEquals(user.getPermanentAddress().getAddress(), "test");
        assertEquals(user.getPermanentAddress().getCity(), "San Mateo");
        assertEquals(user.getPermanentAddress().getState(), "Rizal");
        assertEquals(user.getPermanentAddress().getProvince(), "test");
        assertEquals(user.getPermanentAddress().getRegion(), "IV-A");
        assertEquals(user.getPermanentAddress().getCountry(), "Phillipines");
        assertEquals(user.getPermanentAddress().getPostalCode(), "1990");
    }

}
