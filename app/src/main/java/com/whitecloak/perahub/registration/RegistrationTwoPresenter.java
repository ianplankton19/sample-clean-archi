package com.whitecloak.perahub.registration;

import com.whitecloak.perahub.commons.base.BaseMvpPresenter;
import com.whitecloak.perahub.domain.form.interactor.UpdateRegistrationForm;

import java.util.regex.Pattern;

import javax.inject.Inject;

import io.reactivex.functions.Action;

public class RegistrationTwoPresenter extends BaseMvpPresenter<RegistrationTwoView> {

    private final UpdateRegistrationForm updateRegistrationForm;

    @Inject
    public RegistrationTwoPresenter(UpdateRegistrationForm updateRegistrationForm) {
        this.updateRegistrationForm = updateRegistrationForm;
    }

    private static final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    private String firstName = "", lastName = "", middleName = "", birthday = "", nationality = "",
            address = "", email = "";


    public void setFirstName(String firstName) {
        this.firstName = firstName;
        if (validateFirstname()) {
            checkUpdates();
        }

    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
        if (validateLastName()) {
            checkUpdates();
        }
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
        if (validateMiddleName()) {
            checkUpdates();
        }
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
        if (validateBirthday()) {
            checkUpdates();
        }
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
        if (validateNationality()) {
            checkUpdates();
        }
    }

    public void setAddress(String address) {
        this.address = address;
        if (validateAddress()) {
            checkUpdates();
        }
    }

    public void setEmail(String email) {
        this.email = email;
        if (validateEmail()) {
            checkUpdates();
        }
    }

    private boolean validateFirstname() {
        if (firstName.equals("")) {
            getView().setFirstnameError("first name is required");
            return false;
        }
        getView().clearFirstnameError();
        return true;
    }

    private boolean validateMiddleName() {
        if (middleName.equals("")) {
            getView().setMiddlenameError("middle name is required");
            return false;
        }
        getView().clearMiddlenameError();
        return true;
    }

    private boolean validateLastName() {
        if (lastName.equals("")) {
            getView().setLastName("last name is required");
            return false;
        }
        getView().clearLastnameError();
        return true;
    }

    private boolean validateBirthday() {
        if (birthday.equals("")) {
            getView().setBirthdayError("birthday is required");
            return false;
        }
        getView().clearBirthdayError();
        return true;
    }

    private boolean validateNationality() {
        if (nationality.equals("")) {
            getView().setNationality("nationality is required");
            return false;
        }
        getView().clearNationalityError();
        return true;
    }

    private boolean validateAddress() {
        if (address.equals("")) {
            getView().setAddress("address is required");
            return false;
        }
        getView().clearAddressError();
        return true;
    }

    private boolean validateEmail() {

        if (email.equals("")) {
            getView().setEmailError("email is required");
            return false;
        }

        getView().clearEmailError();
        return true;
    }


    public void checkUpdates() {
        getView().enableNextPage(!firstName.equals("") && !middleName.equals("") &&
                !lastName.equals("") && !birthday.equals("") &&
                !nationality.equals("") && !address.equals(""));
    }

    public void submit() {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\." +
                "[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email.trim());
        if (!m.matches()) {
            getView().setEmailError("email is not valid");
            return;
        }

        updateRegistrationForm.info(firstName, middleName, lastName, birthday,
                address, nationality, email).subscribe(() -> getView().navigateToNextFragment());

    }
}
