package com.whitecloak.perahub.registration;

import com.whitecloak.perahub.commons.base.BaseMvpPresenter;
import com.whitecloak.perahub.domain.interactors.registration.RequestRegisterOtpUseCase;

import javax.inject.Inject;

public class PhoneInputPresenter extends BaseMvpPresenter<PhoneInputView> {

    private final RequestRegisterOtpUseCase requestRegisterOtpUseCase;

    private String mobileNo;


    @Inject
    public PhoneInputPresenter(RequestRegisterOtpUseCase requestRegisterOtpUseCase) {
        this.requestRegisterOtpUseCase = requestRegisterOtpUseCase;
    }

    public void setMobileNumber(String mobileNo) {
        this.mobileNo = mobileNo;
        checkPhoneDetailsValue();
    }

    public void checkPhoneDetailsValue() {
        if (mobileNo.trim().length() == 10) {
            checkNumberIfValid();
            return;
        }
        getView().enableContinueButton(false);
    }

    public void sendRequestOtp(String mobile) {
        requestRegisterOtpUseCase.execute(mobile)
                .subscribe(() -> {
                    getView().dismissLoading();
                    getView().navigateToNextFragment(mobile);
                }, throwable -> {
                    getView().dismissLoading();
                    getView().displayError(throwable.getLocalizedMessage());
                });
    }

    public void checkNumberIfValid() {
        getView().enableContinueButton(true);
    }

}
