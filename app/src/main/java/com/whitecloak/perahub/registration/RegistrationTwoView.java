package com.whitecloak.perahub.registration;

import com.whitecloak.perahub.commons.base.BaseView;

public interface RegistrationTwoView extends BaseView {

    void enableNextPage(boolean checker);

    void setFirstname(String firstname);
    void setMiddleName(String middleName);
    void setLastName(String lastName);
    void setBirthday(String birthday);
    void setNationality(String nationality);
    void setAddress(String address);
    void setEmail(String email);

    void setFirstnameError(String message);
    void setMiddlenameError(String message);
    void setLastnameError(String message);
    void setBirthdayError(String message);
    void setNationalityError(String message);
    void setAddressError(String message);
    void setEmailError(String message);
    void clearFirstnameError();
    void clearMiddlenameError();
    void clearLastnameError();
    void clearBirthdayError();
    void clearNationalityError();
    void clearAddressError();
    void clearEmailError();

    void navigateToNextFragment();

}
