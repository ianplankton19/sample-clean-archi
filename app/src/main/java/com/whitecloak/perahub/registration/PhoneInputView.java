package com.whitecloak.perahub.registration;

import com.whitecloak.perahub.commons.base.BaseView;

public interface PhoneInputView extends BaseView {

    void enableContinueButton(boolean checker);
    void navigateToNextFragment(String mobile);
    void displayError(String message);

    void showLoading();
    void dismissLoading();

}
