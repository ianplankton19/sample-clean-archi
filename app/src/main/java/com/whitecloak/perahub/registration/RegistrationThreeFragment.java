package com.whitecloak.perahub.registration;

import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.afollestad.materialdialogs.MaterialDialog;
import com.whitecloak.perahub.R;
import com.whitecloak.perahub.commons.base.BaseRegistrationFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTextChanged;


public class RegistrationThreeFragment extends
        BaseRegistrationFragment<RegistrationThreeView, RegistrationThreePresenter>
        implements RegistrationThreeView {


    private MaterialDialog materialDialog;

    @Inject
    RegistrationThreePresenter registrationThreePresenter;

    @BindView(R.id.btnRegisterCard)
    Button btnRegisterCard;

    @BindView(R.id.btnRegisterNoCard)
    Button btnRegisterNoCard;

    @BindView(R.id.etCardNo)
    EditText etCardNo;

    @OnTextChanged(value = R.id.etCardNo,
            callback = OnTextChanged.Callback.TEXT_CHANGED)
    void etCardNoBehavior(Editable editable) {
        registrationThreePresenter.setCardNo(editable.toString());
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_registration_three;
    }

    @NonNull
    @Override
    protected RegistrationThreePresenter getMvpPresenter() {
        return registrationThreePresenter;
    }

    @NonNull
    @Override
    protected RegistrationThreeView getMvpView() {
        return this;
    }

    @Override
    protected void setupDependencies() {

    }

    @Override
    protected void loadContent() {

    }

    @Override
    protected void setupViews() {
        super.setupViews();
    }


    @Override
    public void setCardNumberFieldError(String message) {
        etCardNo.setError(message);
    }

    @Override
    public void clearCardNumberError() {
        etCardNo.setError(null);
    }

    @Override
    public void enableRegisterNumberButton(boolean checker) {
        btnRegisterCard.setEnabled(checker);
    }

    @Override
    public void showLoading() {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(getActivity())
                .title("")
                .content("Please wait..")
                .progress(true, 0);
        materialDialog = builder.build();
        materialDialog.show();
    }

    @Override
    public void dismissLoading() {
        materialDialog.dismiss();
    }

    @Override
    public void networkError(String message) {
        new MaterialDialog.Builder(getActivity())
                .title("Error")
                .content(message)
                .positiveText("OK")
                .show();
    }

    @Override
    public void navigateToSuccessPage() {
        RegistrationSuccessFragment registrationSuccessFragment = new RegistrationSuccessFragment();
        replaceFragmentAddBackStack(registrationSuccessFragment, "fragment_success");
        registrationActivityView.setPageTitle("Registration Complete");
    }

    @OnClick({R.id.btnRegisterCard, R.id.btnRegisterNoCard})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnRegisterCard:
                registrationThreePresenter.submit(true);
                break;
            case R.id.btnRegisterNoCard:
                registrationThreePresenter.submit(false);
                break;
        }
    }




}
