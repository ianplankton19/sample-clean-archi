package com.whitecloak.perahub.registration;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.whitecloak.perahub.R;
import com.whitecloak.perahub.commons.base.BaseRegistrationFragment;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import butterknife.Unbinder;


public class RegistrationOneFragment extends BaseRegistrationFragment
        <RegistrationOneView, RegistrationOnePresenter> implements RegistrationOneView {

    @BindView(R.id.etUsername)
    EditText etUsername;

    @BindView(R.id.etPassword)
    EditText etPassword;

    @BindView(R.id.vFirstIndicator)
    View vFirstIndicator;

    @BindView(R.id.vSecondIndicator)
    View vSecondIndicator;

    @BindView(R.id.vThirdIndicator)
    View vThirdIndicator;

    @BindView(R.id.vFourthIndicator)
    View vFourthIndicator;

    @BindView(R.id.vFifthIndicator)
    View vFifthIndicator;

    @BindView(R.id.btnGetStarted)
    Button btnGetStarted;

    @BindView(R.id.vCircleOne)
    View vCircleOne;

    @BindView(R.id.vCircleTwo)
    View vCircleTwo;

    @BindView(R.id.vCircleThree)
    View vCircleThree;

    @BindView(R.id.vCircleFour)
    View vCircleFour;

    @BindView(R.id.vCircleFive)
    View vCircleFive;

    @Inject
    RegistrationOnePresenter registrationOnePresenter;

    private String username = "", password = "";

    private ArrayList<View> viewArrayList = new ArrayList<>();

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_registration_one;
    }

    @NonNull
    @Override
    protected RegistrationOnePresenter getMvpPresenter() {
        return registrationOnePresenter;
    }

    @NonNull
    @Override
    protected RegistrationOneView getMvpView() {
        return this;
    }

    @Override
    protected void setupDependencies() {

    }

    @Override
    protected void setupViews() {
        super.setupViews();
//
        viewArrayList.add(vFirstIndicator);
        viewArrayList.add(vSecondIndicator);
        viewArrayList.add(vThirdIndicator);
        viewArrayList.add(vFourthIndicator);
        viewArrayList.add(vFifthIndicator);

    }

    @OnTextChanged(value = R.id.etUsername,
            callback = OnTextChanged.Callback.TEXT_CHANGED)
    void etUserBehavior(Editable editable) {
        registrationOnePresenter.setUsername(editable.toString());
    }

    @OnTextChanged(value = R.id.etPassword,
            callback = OnTextChanged.Callback.TEXT_CHANGED)
    void etPAsswordBehavior(Editable editable) {
        registrationOnePresenter.setPassword(editable.toString());
    }

    @Override
    protected void loadContent() {

    }

    @Override
    public void enableNextButton(boolean checker) {
        btnGetStarted.setEnabled(checker);
    }

    @Override
    public void updateIndicator(int points) {
//        RESET color indicators
        for (View view : viewArrayList) {
            view.setBackgroundColor(getResources().getColor(R.color.gray_alpha));
        }
//        UPDATE color indicators
        for (int i = 0; points > i; i++) {
            viewArrayList.get(i).setBackgroundColor(getResources().getColor(R.color.green_indicator));
        }
    }

    @Override
    public void setUserNameField(String username) {
        etUsername.setText(username);
    }

    @Override
    public void setPasswordField(String password) {
        etPassword.setText(password);
    }

    @Override
    public void firstDirectionEnabled(boolean checker) {
        vCircleOne.setBackgroundResource(checker ?
                R.drawable.layout_circle_green : R.drawable.layout_circle_gray);
    }

    @Override
    public void secondDirectionEnabled(boolean checker) {
        vCircleTwo.setBackgroundResource(checker ?
                R.drawable.layout_circle_green : R.drawable.layout_circle_gray);
    }

    @Override
    public void thrdDirectionEnabled(boolean checker) {
        vCircleThree.setBackgroundResource(checker ?
                R.drawable.layout_circle_green : R.drawable.layout_circle_gray);
    }

    @Override
    public void fourthDirectionEnabled(boolean checker) {
        vCircleFour.setBackgroundResource(checker ?
                R.drawable.layout_circle_green : R.drawable.layout_circle_gray);
    }

    @Override
    public void fifthDirectionEnabled(boolean checker) {
        vCircleFive.setBackgroundResource(checker ?
                R.drawable.layout_circle_green : R.drawable.layout_circle_gray);
    }

    @Override
    public void setUsernameFieldError(String message) {
        etUsername.setError(message);
    }

    @Override
    public void setPasswordFieldError(String message) {
        etPassword.setError(message);
    }

    @Override
    public void clearUsernameError() {
        etUsername.setError(null);
    }

    @Override
    public void clearPasswordError() {
        etPassword.setError(null);
    }

    @Override
    public void navigateToNextFragment() {
        RegistrationTwoFragment registrationTwoFragment = new RegistrationTwoFragment();
        replaceFragmentAddBackStack(registrationTwoFragment, "fragment_two");
        registrationActivityView.setPageTitle("Account Details");
    }

    @Override
    public void navigateToDisplaySecurityFragment() {
        SecurityDisclaimerFragment securityDisclaimerFragment = new SecurityDisclaimerFragment();
        replaceFragmentAddBackStack(securityDisclaimerFragment, "fragment_security");
        registrationActivityView.setPageTitle("Security Disclaimer");
        registrationActivityView.setPageToolbar(R.drawable.ic_close_logo);
    }

    @OnClick({R.id.tvSecurityDisclaimer, R.id.btnGetStarted})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tvSecurityDisclaimer:
                navigateToDisplaySecurityFragment();
                break;
            case R.id.btnGetStarted:
                registrationOnePresenter.submit();
                break;
        }
    }

}