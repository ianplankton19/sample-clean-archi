package com.whitecloak.perahub.registration;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.whitecloak.perahub.R;
import com.whitecloak.perahub.commons.base.BaseMvpActivity;
import com.whitecloak.perahub.otp.OtpActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class PhoneInputActivity extends BaseMvpActivity<PhoneInputView, PhoneInputPresenter> implements PhoneInputView {

    public static Intent getIntent(Context context) {
        return new Intent(context, PhoneInputActivity.class);
    }

    private MaterialDialog materialDialog;

    @Inject
    PhoneInputPresenter phoneInputPresenter;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.vToolbarUnderline)
    View vToolbarUnderline;

    @BindView(R.id.etMobileNumber)
    EditText etMobileNumber;

    @BindView(R.id.btnGetStarted)
    Button btnGetStarted;

    @OnTextChanged(value = R.id.etMobileNumber,
            callback = OnTextChanged.Callback.TEXT_CHANGED)
    void editTextMobileBehavior(Editable editable) {
        phoneInputPresenter.setMobileNumber(String.valueOf(editable));
    }


    @Override
    protected int getLayoutRes() {
        return R.layout.activity_phone_input;
    }

    @NonNull
    @Override
    protected PhoneInputView getMvpView() {
        return this;
    }

    @NonNull
    @Override
    protected PhoneInputPresenter getMvpPresenter() {
        return phoneInputPresenter;
    }

    @Override
    protected void setupDependencies() {

    }

    @Override
    protected void setupViews() {
        initToolbar();
    }

    @Override
    protected void loadContent() {

    }

    @Override
    public void enableContinueButton(boolean checker) {
        btnGetStarted.setEnabled(checker);
    }

    @Override
    public void navigateToNextFragment(String mobile) {
        startActivity(OtpActivity.getIntent(this, mobile, "",
                "", "Registration"));
        finish();
    }

    @Override
    public void displayError(String message) {
       providePositiveDialog(this, message);
    }

    @Override
    public void showLoading() {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                .title("Sending Request")
                .content("please wait..")
                .progress(true, 0);
        materialDialog = builder.build();
        materialDialog.show();
    }

    @Override
    public void dismissLoading() {
        materialDialog.dismiss();
    }

    @OnClick(R.id.btnGetStarted)
    public void onViewClicked() {
        showLoading();
        phoneInputPresenter.sendRequestOtp("0" + etMobileNumber.getText().toString());
    }



    private void initToolbar() {
        setSupportActionBar(toolbar);
        tvTitle.setText("Register");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(R.drawable.ic_action_back);
        toolbar.setNavigationOnClickListener(v -> {
            onBackPressed();
        });

    }
}
