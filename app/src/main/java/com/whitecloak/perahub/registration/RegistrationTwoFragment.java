package com.whitecloak.perahub.registration;

import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.whitecloak.perahub.R;
import com.whitecloak.perahub.commons.base.BaseRegistrationFragment;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class RegistrationTwoFragment extends BaseRegistrationFragment<RegistrationTwoView, RegistrationTwoPresenter>
        implements RegistrationTwoView, DatePickerDialog.OnDateSetListener {

    @Inject
    RegistrationTwoPresenter registrationTwoPresenter;

    @BindView(R.id.etFirstName)
    EditText etFirstName;

    @BindView(R.id.etMiddleName)
    EditText etMiddleName;

    @BindView(R.id.etLastName)
    EditText etLastName;

    @BindView(R.id.etBirthday)
    EditText etBirthday;

    @BindView(R.id.etNationality)
    EditText etNationality;

    @BindView(R.id.etAddress)
    EditText etAddress;

    @BindView(R.id.etEmail)
    EditText etEmail;

    @BindView(R.id.btnContinue)
    Button btnContinue;

    @OnTextChanged(value = R.id.etFirstName,
            callback = OnTextChanged.Callback.TEXT_CHANGED)
    void etFirstNameBehavior(Editable editable) {
        registrationTwoPresenter.setFirstName(editable.toString());
    }

    @OnTextChanged(value = R.id.etMiddleName,
            callback = OnTextChanged.Callback.TEXT_CHANGED)
    void etMiddleNameBehavior(Editable editable) {
        registrationTwoPresenter.setMiddleName(editable.toString());
    }

    @OnTextChanged(value = R.id.etLastName,
            callback = OnTextChanged.Callback.TEXT_CHANGED)
    void etLastNameBehavior(Editable editable) {
        registrationTwoPresenter.setLastName(editable.toString());
    }

    @OnTextChanged(value = R.id.etNationality,
            callback = OnTextChanged.Callback.TEXT_CHANGED)
    void etNationalityBehavior(Editable editable) {
        registrationTwoPresenter.setNationality(editable.toString());
    }

    @OnTextChanged(value = R.id.etAddress,
            callback = OnTextChanged.Callback.TEXT_CHANGED)
    void etAddressBehavior(Editable editable) {
        registrationTwoPresenter.setAddress(editable.toString());
    }

    @OnTextChanged(value = R.id.etEmail,
            callback = OnTextChanged.Callback.TEXT_CHANGED)
    void etEmailBehavior(Editable editable) {
        registrationTwoPresenter.setEmail(editable.toString());
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_registration_two;
    }

    @NonNull
    @Override
    protected RegistrationTwoPresenter getMvpPresenter() {
        return registrationTwoPresenter;
    }

    @NonNull
    @Override
    protected RegistrationTwoView getMvpView() {
        return this;
    }

    @Override
    protected void setupDependencies() {

    }

    @Override
    protected void setupViews() {
        super.setupViews();
    }

    @Override
    protected void loadContent() {

    }


    @Override
    public void enableNextPage(boolean checker) {
        btnContinue.setEnabled(checker);
    }

    @Override
    public void setFirstname(String firstname) {
        etFirstName.setText(firstname);
    }

    @Override
    public void setMiddleName(String middleName) {
        etMiddleName.setText(middleName);
    }

    @Override
    public void setLastName(String lastName) {
        etLastName.setText(lastName);
    }

    @Override
    public void setBirthday(String birthday) {
        etBirthday.setText(birthday);
    }

    @Override
    public void setNationality(String nationality) {
        etNationality.setText(nationality);
    }

    @Override
    public void setAddress(String address) {
        etAddress.setText(address);
    }

    @Override
    public void setEmail(String email) {
        etEmail.setText(email);
    }

    @Override
    public void setFirstnameError(String message) {
        etFirstName.setError(message);
    }

    @Override
    public void setMiddlenameError(String message) {
        etMiddleName.setError(message);
    }

    @Override
    public void setLastnameError(String message) {
        etLastName.setError(message);
    }

    @Override
    public void setBirthdayError(String message) {
        etBirthday.setError(message);
    }

    @Override
    public void setNationalityError(String message) {
        etNationality.setError(message);
    }

    @Override
    public void setAddressError(String message) {
        etAddress.setError(message);
    }

    @Override
    public void setEmailError(String message) {
        etEmail.setError(message);
    }

    @Override
    public void clearFirstnameError() {
        etFirstName.setError(null);
    }

    @Override
    public void clearMiddlenameError() {
        etMiddleName.setError(null);
    }

    @Override
    public void clearLastnameError() {
        etLastName.setError(null);
    }

    @Override
    public void clearBirthdayError() {
        etBirthday.setError(null);
    }

    @Override
    public void clearNationalityError() {
        etNationality.setError(null);
    }

    @Override
    public void clearAddressError() {
        etAddress.setError(null);
    }

    @Override
    public void clearEmailError() {
        etEmail.setError(null);
    }

    @Override
    public void navigateToNextFragment() {
        RegistrationThreeFragment registrationThreeFragment = new RegistrationThreeFragment();
        replaceFragmentAddBackStack(registrationThreeFragment, "fragment_three");
        registrationActivityView.setPageTitle("Register PERA HUB Visa Card");
    }


    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

        NumberFormat formatter = new DecimalFormat("00");

        String birthday = formatter.format(monthOfYear + 1) + "/"
                + formatter.format(dayOfMonth)  + "/" + year;
        registrationTwoPresenter.setBirthday(birthday);
        etBirthday.setText(birthday);
    }


    @OnClick({R.id.etBirthday, R.id.btnContinue})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.etBirthday:
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.setAccentColor(ContextCompat.getColor(getActivity(), R.color.text_primary));
                dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
                break;
            case R.id.btnContinue:
                registrationTwoPresenter.submit();
                break;
        }
    }


}
