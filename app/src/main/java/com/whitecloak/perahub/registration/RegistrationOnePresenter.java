package com.whitecloak.perahub.registration;

import android.support.annotation.NonNull;
import android.util.Log;

import com.whitecloak.perahub.commons.base.BaseMvpPresenter;
import com.whitecloak.perahub.domain.form.interactor.UpdateRegistrationForm;

import javax.inject.Inject;

import io.reactivex.functions.Action;

public class RegistrationOnePresenter extends BaseMvpPresenter<RegistrationOneView> {

    private final UpdateRegistrationForm updateRegistrationForm;

    private String username = "", password = "";
    int points = 0;

    @Inject
    public RegistrationOnePresenter(UpdateRegistrationForm updateRegistrationForm) {
        this.updateRegistrationForm = updateRegistrationForm;
    }

    public void setUsername(String username) {
        this.username = username;
        if (validateUserName()) {
            checkValues();
            return;
        }

    }

    public void setPassword(String password) {
        this.password = password;
        if (validatePassword()) {
            checkPasswordStrength();
            return;
        }
    }

    public void checkPasswordStrength() {
        boolean filter1 = false, filter2 = false, filter3 = false, filter4 = false, filter5 = false;
         points = 0;

        if (!password.equals("")) {
            points += 1;
            filter1 = true;
        }
        if (password.length() >= 8) {
            points += 1;
            filter2 = true;
        }
        if (password.matches(".*[a-zA-Z]+.*")) {

            Log.i("TAG", "yehey1!");
            points += 1;
            filter3 = true;
        }
        if (password.matches(".*\\d.*")) {
            Log.i("TAG", "yehey2!");
            points += 1;
            filter4 = true;
        }
        if (!password.matches("[a-zA-Z0-9]*")) {
            Log.i("TAG", "yehey3!");
            points += 1;
            filter5 = true;
        }

        getView().updateIndicator(points);
        getView().firstDirectionEnabled(filter1);
        getView().secondDirectionEnabled(filter2);
        getView().thrdDirectionEnabled(filter3);
        getView().fourthDirectionEnabled(filter4);
        getView().fifthDirectionEnabled(filter5);
        checkValues();

    }

    private boolean validateUserName() {
        if (username.equals("")) {
            getView().setUsernameFieldError("Username is required");
            return false;
        }
        getView().clearUsernameError();
        return true;
    }

    private boolean validatePassword() {
        if (password.equals("")) {
            getView().setPasswordFieldError("Password is required");
            return false;
        }
        getView().clearPasswordError();
        return true;
    }

    public void checkValues() {
        getView().enableNextButton(!username.equals("") && points == 5);

    }

    public void submit() {
        updateRegistrationForm.credentials(username, password).subscribe(()
                -> getView().navigateToNextFragment());

    }

}
