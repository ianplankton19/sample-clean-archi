package com.whitecloak.perahub.registration;

import com.whitecloak.perahub.commons.base.BaseView;

public interface RegistrationThreeView extends BaseView {

    void setCardNumberFieldError(String message);
    void clearCardNumberError();

    void enableRegisterNumberButton(boolean checker);

    void showLoading();
    void dismissLoading();
    void networkError(String message);
    void navigateToSuccessPage();
}
