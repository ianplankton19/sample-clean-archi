package com.whitecloak.perahub.registration;

import com.whitecloak.perahub.commons.base.BaseView;

public interface RegistrationOneView extends BaseView {

    void enableNextButton(boolean checker);
    void updateIndicator(int points);


    void setUserNameField(String username);
    void setPasswordField(String password);

    void firstDirectionEnabled(boolean checker);
    void secondDirectionEnabled(boolean checker);
    void thrdDirectionEnabled(boolean checker);
    void fourthDirectionEnabled(boolean checker);
    void fifthDirectionEnabled(boolean checker);

    void setUsernameFieldError(String message);
    void setPasswordFieldError(String message);

    void clearUsernameError();
    void clearPasswordError();

    void navigateToNextFragment();
    void navigateToDisplaySecurityFragment();

}