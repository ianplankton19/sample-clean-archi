package com.whitecloak.perahub.registration;

import com.whitecloak.perahub.commons.base.BaseMvpPresenter;
import com.whitecloak.perahub.commons.model.RegistrationParam;

import javax.inject.Inject;

public class RegistrationActivityPresenter extends BaseMvpPresenter<RegistrationActivityView> {

    private RegistrationParam registrationParam;

    @Inject
    public RegistrationActivityPresenter() {
        registrationParam = new RegistrationParam();
    }

    public void setPhoneNumber(String phoneNumber) {
        registrationParam.setPhoneNo(phoneNumber);
    }

    public void setOtp(String otp) {
        registrationParam.setOtp(otp);
    }

    public void setUserPassword(String username, String password) {
        registrationParam.setUsername(username);
        registrationParam.setPassword(password);
    }

    public void setAccountDetails(String firstName, String middleName, String lastName,
                                  String birthday, String nationality, String address,
                                  String email) {
        registrationParam.setFirstName(firstName);
        registrationParam.setMiddleName(middleName);
        registrationParam.setLastName(lastName);
        registrationParam.setBirthday(birthday);
        registrationParam.setNationality(nationality);
        registrationParam.setAddress(address);
        registrationParam.setEmail(email);
    }

    public void setAccountNo(String accountNo) {
        registrationParam.setAccountNo(accountNo);
    }

    public RegistrationParam getRegistrationParam() {
        return registrationParam;
    }

    public void setRegistrationParam(RegistrationParam registrationParam) {
        this.registrationParam = registrationParam;
    }
}
