package com.whitecloak.perahub.registration;


import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.whitecloak.perahub.R;
import com.whitecloak.perahub.commons.base.BaseMvpActivity;
import com.whitecloak.perahub.commons.model.RegistrationParam;

import javax.inject.Inject;

import butterknife.BindView;

public class RegistrationActivity extends BaseMvpActivity<RegistrationActivityView, RegistrationActivityPresenter> implements RegistrationActivityView {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.vToolbarUnderline)
    View vToolbarUnderline;

    @BindView(R.id.flFragmentContainer)
    FrameLayout flFragmentContainer;

    @BindView(R.id.svParent)
    ScrollView svParent;

    @Inject
    RegistrationActivityPresenter registrationActivityPresenter;

    public FragmentTransaction fragmentTransaction;

    public static Intent getIntent(Context context) {
        return new Intent(context, RegistrationActivity.class);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_registration;
    }

    @NonNull
    @Override
    protected RegistrationActivityView getMvpView() {
        return this;
    }

    @NonNull
    @Override
    protected RegistrationActivityPresenter getMvpPresenter() {
        return registrationActivityPresenter;
    }

    @Override
    protected void setupDependencies() {

    }

    @Override
    protected void setupViews() {
        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        RegistrationOneFragment registrationTwoFragment = new RegistrationOneFragment();
        fragmentTransaction.add(R.id.flFragmentContainer, registrationTwoFragment, "RegisterParam");
        fragmentTransaction.addToBackStack("fragment_one");
        fragmentTransaction.commit();
        initToolbar();
    }

    @Override
    protected void loadContent() {

    }

    @Override
    public void onBackPressed() {
        SecurityDisclaimerFragment securityDisclaimerFragment = (SecurityDisclaimerFragment)
                getSupportFragmentManager().findFragmentByTag("fragment_security");
        FragmentManager fm = getSupportFragmentManager();
//
//        if (fm.getBackStackEntryCount() > 1) {
//            fm.popBackStack();
//        } else {
        if (securityDisclaimerFragment != null && securityDisclaimerFragment.isVisible()) {
            fm.popBackStack();
            return;
        }
            finish();
//        }
    }

    @Override
    public void scrollViewScrollTop() {
        svParent.smoothScrollTo(0, 0);
    }

    @Override
    public void setPageTitle(String title) {
        tvTitle.setText(title);
    }

    @Override
    public void setPageToolbar(int drawable) {
        toolbar.setNavigationIcon(drawable);
    }


    private void initToolbar() {
        setSupportActionBar(toolbar);
        tvTitle.setText("Register");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(R.drawable.ic_action_back);
        toolbar.setNavigationOnClickListener(v -> {
            onBackPressed();
        });

    }

}
