package com.whitecloak.perahub.registration;

import com.whitecloak.perahub.commons.base.BaseMvpPresenter;
import com.whitecloak.perahub.domain.form.interactor.UpdateRegistrationForm;
import com.whitecloak.perahub.domain.interactors.registration.RegisterUseCase;

import javax.inject.Inject;

public class RegistrationThreePresenter extends BaseMvpPresenter<RegistrationThreeView> {

    private final UpdateRegistrationForm updateRegistrationForm;
    private final RegisterUseCase registerUseCase;

    @Inject
    public RegistrationThreePresenter(RegisterUseCase registerUseCase,
                                      UpdateRegistrationForm updateRegistrationForm) {
        this.updateRegistrationForm = updateRegistrationForm;
        this.registerUseCase = registerUseCase;
    }

    private String cardNo = "";

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
        if (validateCardNo()) {
            checkCardDetails();
        }
    }

    private boolean validateCardNo() {
        if (cardNo.equals("")) {
            return false;
        }
        return true;
    }

    public void checkCardDetails() {
        getView().enableRegisterNumberButton(cardNo.length() == 12);
    }

    public void submit(boolean registerCard) {
        getView().showLoading();
        updateRegistrationForm.cardDetails(registerCard ? cardNo : "").subscribe();
        registerAccount();
    }

    public void registerAccount() {

        registerUseCase.execute(updateRegistrationForm.getForm()).subscribe(user -> {
            getView().dismissLoading();
            getView().navigateToSuccessPage();
        }, throwable -> {
            getView().dismissLoading();
            getView().networkError(throwable.getLocalizedMessage());
        });
    }
}
