package com.whitecloak.perahub.registration;

import com.whitecloak.perahub.commons.base.BaseView;

public interface RegistrationActivityView extends BaseView {

    void scrollViewScrollTop();

    void setPageTitle(String title);

    void setPageToolbar(int drawable);

}
