package com.whitecloak.perahub.registration;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.whitecloak.perahub.R;

public class SecurityDisclaimerFragment extends Fragment {

    private RegistrationActivityView registrationActivityView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_security_disclaimer, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        registrationActivityView = (RegistrationActivity) getActivity();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        registrationActivityView.setPageTitle("Register");
        registrationActivityView.setPageToolbar(R.drawable.ic_action_back);
    }
}
