package com.whitecloak.perahub.otp;

import android.os.CountDownTimer;

import com.whitecloak.perahub.commons.base.BaseMvpPresenter;
import com.whitecloak.perahub.domain.interactors.registration.ValidateRegisterOtpUseCase;
import com.whitecloak.perahub.domain.interactors.user.LoginUseCase;
import com.whitecloak.perahub.domain.interactors.user.ValidateLoginUseCase;

import javax.inject.Inject;

public class OtpPresenter extends BaseMvpPresenter<OtpView> {

    private String otpCode = "";
    private String otpType = "";
    private String username = "";
    private String password = "";
    private String mobileNo = "";

    private final ValidateRegisterOtpUseCase validateRegisterOtpUseCase;
    private final ValidateLoginUseCase validateLoginUseCase;
    private final LoginUseCase loginUseCase;


    @Inject
    public OtpPresenter(ValidateRegisterOtpUseCase validateRegisterOtpUseCase,
                        ValidateLoginUseCase validateLoginUseCase,
                        LoginUseCase loginUseCase) {
        this.validateRegisterOtpUseCase = validateRegisterOtpUseCase;
        this.validateLoginUseCase = validateLoginUseCase;
        this.loginUseCase = loginUseCase;
    }

    public void setInfoForLoginResend(String username, String password) {
        this.username = username;
        this.password = password;
    }


    public void getOtpType(String otpType) {
        this.otpType = otpType;
    }


    public void renderValue(String value) {


        if (value.length() > 0) {


            for (int i = 0; value.length() > i; i++) {

                String tempText = value.substring(i, i + 1);
                getView().updateString(i, tempText);

            }

            if (value.length() == 6) {
                checkOtpValue(value);
            }

        }

    }

    public void resetString(String value) {
        if (value.length() < 6) {
            for (int i = value.length(); 6 > i; i++) {
                getView().resetString(i);

            }
        }
    }

    public void checkOtpValue(String value) {
        otpCode = value;
        getView().showLoading();
        if (otpType.equals("Registration")) {
            validateRegisterOtpCode();
            return;
        }
        validateLoginOtpCode();

    }

    public void validateRegisterOtpCode() {
        validateRegisterOtpUseCase.execute(otpCode)
                .subscribe(() -> {
                    if (otpType.equals("Registration")) {
                        getView().dismissLoading();
                        getView().navigateToRegistrationPage();
                        return;
                    }
                    getView().navigateToDashboardPage();
                }, throwable -> {
                    getView().dismissLoading();
                    getView().errorOtpInputAlert();

                });
    }

    public void validateLoginOtpCode() {
        validateLoginUseCase.execute(otpCode)
                .subscribe(user -> {
                    getView().dismissLoading();
                    getView().navigateToDashboardPage();
                }, throwable -> {
                    getView().dismissLoading();
                    getView().errorOtpInputAlert();

                });

    }

    public void requestCodeResend() {
        loginUseCase.execute(username, password).subscribe(s -> {

        }, throwable -> {

        });
    }


    public void startCountDown() {
        new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {
                if (getView() != null)
                getView().setCounterText("" +millisUntilFinished / 1000);

            }

            public void onFinish() {
//               requestCodeResend();
            }

        }.start();
    }


}
