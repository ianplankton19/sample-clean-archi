package com.whitecloak.perahub.otp;

import com.whitecloak.perahub.commons.base.BaseView;

public interface OtpView extends BaseView {

    void updateString(int index, String value);
    void resetString(int index);
    void setStringEmpty();
    void navigateToRegistrationPage();
    void navigateToDashboardPage();
    void errorOtpInputAlert();
    void showLoading();
    void dismissLoading();
    void networkError(String message);

    void initializeCountDown();
    void setCounterText(String text);
}
