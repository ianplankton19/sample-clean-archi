package com.whitecloak.perahub.otp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.whitecloak.perahub.R;
import com.whitecloak.perahub.commons.base.BaseMvpActivity;
import com.whitecloak.perahub.dashboard.DashboardActivity;
import com.whitecloak.perahub.registration.RegistrationActivity;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTextChanged;

public class OtpActivity extends BaseMvpActivity<OtpView, OtpPresenter> implements OtpView, View.OnKeyListener {
    //Vars need for resend
    public static final String ARGS_PHONENO = "ARGS_PHONENO";
    public static final String ARGS_USERNAME = "ARGS_USERNAME";
    public static final String ARGS_PASSWORD = "ARGS_PASSWORD";
    //-----//
    public static final String ARGS_OTP_TYPE = "ARGS_OTP_TYPE";


    public static Intent getIntent(Context context, String phoneNo, String username,
                                   String password, String otpType) {
        Intent intent = new Intent(context, OtpActivity.class);
        intent.putExtra(ARGS_USERNAME, username);
        intent.putExtra(ARGS_PASSWORD, password);
        intent.putExtra(ARGS_PHONENO, phoneNo);
        intent.putExtra(ARGS_OTP_TYPE, otpType);
        return intent;
    }

    private MaterialDialog materialDialog;

    private ArrayList<TextView> textViewArrayList = new ArrayList<>();
    private ArrayList<View> viewArrayList = new ArrayList<>();

    @Inject
    OtpPresenter otpPresenter;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.etInvisibleEditText)
    EditText etInvisibleEditText;

    @BindView(R.id.vToolbarUnderline)
    View vToolbarUnderline;

    @BindView(R.id.tvCounter)
    TextView tvCounter;

    @BindView(R.id.vFirstDot)
    View vFirstDot;

    @BindView(R.id.vSecondDot)
    View vSecondDot;

    @BindView(R.id.vThirdDot)
    View vThirdDot;

    @BindView(R.id.vFourthDot)
    View vFourthDot;

    @BindView(R.id.vFifthDot)
    View vFifthDot;

    @BindView(R.id.vSixthDot)
    View vSixthDot;

    @BindView(R.id.tvLabelOne)
    TextView tvLabelOne;

    @BindView(R.id.tvLabelTwo)
    TextView tvLabelTwo;

    @BindView(R.id.tvLabelThree)
    TextView tvLabelThree;

    @BindView(R.id.tvLabelFour)
    TextView tvLabelFour;

    @BindView(R.id.tvLabelFive)
    TextView tvLabelFive;

    @BindView(R.id.tvLabelSix)
    TextView tvLabelSix;

    @BindView(R.id.tvOtpMessage)
    TextView tvOtpMessage;


    @OnTextChanged(value = R.id.etInvisibleEditText,
            callback = OnTextChanged.Callback.TEXT_CHANGED)
    void otpBehavior(Editable editable) {
        otpPresenter.resetString(String.valueOf(editable));
        otpPresenter.renderValue(String.valueOf(editable));
    }


    @Override
    protected int getLayoutRes() {
        return R.layout.activity_otp;
    }

    @NonNull
    @Override
    protected OtpView getMvpView() {
        return this;
    }

    @NonNull
    @Override
    protected OtpPresenter getMvpPresenter() {
        return otpPresenter;
    }

    @Override
    protected void setupDependencies() {

    }

    @Override
    protected void setupViews() {
        initToolbar();
        String mobileNo = getIntent().getExtras().getString(ARGS_PHONENO);

        otpPresenter.startCountDown();
        otpPresenter.setInfoForLoginResend(getIntent().getExtras().getString(ARGS_USERNAME),
                getIntent().getExtras().getString(ARGS_PASSWORD));
        otpPresenter.getOtpType(getIntent().getExtras().getString(ARGS_OTP_TYPE));
        tvOtpMessage.setText(getString(R.string.otp_message) + " " +
                mobileNo.substring(mobileNo.length() - 4));

        textViewArrayList.add(tvLabelOne);
        textViewArrayList.add(tvLabelTwo);
        textViewArrayList.add(tvLabelThree);
        textViewArrayList.add(tvLabelFour);
        textViewArrayList.add(tvLabelFive);
        textViewArrayList.add(tvLabelSix);

        viewArrayList.add(vFirstDot);
        viewArrayList.add(vSecondDot);
        viewArrayList.add(vThirdDot);
        viewArrayList.add(vFourthDot);
        viewArrayList.add(vFifthDot);
        viewArrayList.add(vSixthDot);
        etInvisibleEditText.requestFocus();
    }

    @Override
    protected void loadContent() {

    }

    @Override
    public void updateString(int index, String value) {
        textViewArrayList.get(index).setVisibility(View.VISIBLE);
        textViewArrayList.get(index).setText(value);
        viewArrayList.get(index).setVisibility(View.GONE);
    }

    @Override
    public void resetString(int index) {
        textViewArrayList.get(index).setVisibility(View.INVISIBLE);
        viewArrayList.get(index).setVisibility(View.VISIBLE);
    }

    @Override
    public void setStringEmpty() {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                etInvisibleEditText.setText("");
                etInvisibleEditText.requestFocus();
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            }
        });

    }


    @Override
    public void navigateToRegistrationPage() {
        startActivity(RegistrationActivity.getIntent(this));
        finish();
    }

    @Override
    public void navigateToDashboardPage() {
        startActivity(DashboardActivity.getIntent(this));
        finish();
    }

    @Override
    public void errorOtpInputAlert() {
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(500);
        setStringEmpty();
    }

    @Override
    public void showLoading() {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                .title("Verify OTP")
                .content("please wait..")
                .progress(true, 0);
        materialDialog = builder.build();
        materialDialog.show();
    }

    @Override
    public void dismissLoading() {
        materialDialog.dismiss();
    }

    @Override
    public void networkError(String message) {
        providePositiveDialog(this, message);
    }

    @Override
    public void initializeCountDown() {

    }

    @Override
    public void setCounterText(String text) {
        tvCounter.setText("Did not receive code? RESEND (in " + text + " sec)");
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        tvTitle.setText("OTP Verification");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(R.drawable.ic_action_back);
        toolbar.setNavigationOnClickListener(v -> {
            onBackPressed();
        });

    }


    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
            return false;
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
