package com.whitecloak.perahub.commons.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

public class RegistrationViewPagerAdapter extends FragmentPagerAdapter{

    private ArrayList<Fragment> mFragments = new ArrayList<>();


    public RegistrationViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }


    public void addFragment(Fragment fragment) {
        mFragments.add(fragment);

    }

    @Override
    public Fragment getItem(int position) {
        return mFragments.get(position);
    }

    @Override
    public int getCount() {
        return mFragments.size();
    }
}
