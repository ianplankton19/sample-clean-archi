package com.whitecloak.perahub.commons.injection.modules;

import com.whitecloak.perahub.data.repositories.RegistrationDataRepository;
import com.whitecloak.perahub.data.repositories.UserDataRepository;
import com.whitecloak.perahub.data.repositories.FormDataRepository;
import com.whitecloak.perahub.domain.form.repository.FormRepository;
import com.whitecloak.perahub.domain.repositories.RegistrationRepository;
import com.whitecloak.perahub.domain.repositories.UserRepository;

import dagger.Module;
import dagger.Provides;

/**
 * Created by johneris on 3/10/18.
 */

@Module
public class RepositoryModule {

    @Provides
    RegistrationRepository provideRegistrationRepository(RegistrationDataRepository registrationDataRepository) {
        return registrationDataRepository;
    }

    @Provides
    UserRepository provideUserRepository(UserDataRepository userDataRepository) {
        return userDataRepository;
    }

    @Provides
    FormRepository provideFormRepository(FormDataRepository formDataRepository) {
        return formDataRepository;
    }
}
