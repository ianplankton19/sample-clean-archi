package com.whitecloak.perahub.commons.base;

import com.whitecloak.cleancomponents.mvp.MvpBasePresenter;
import com.whitecloak.cleancomponents.mvp.MvpView;

public abstract class BaseMvpPresenter<V extends MvpView> extends MvpBasePresenter<V> {


}
