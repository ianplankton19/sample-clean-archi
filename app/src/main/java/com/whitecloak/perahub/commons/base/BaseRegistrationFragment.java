package com.whitecloak.perahub.commons.base;


import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.inputmethod.InputMethodManager;

import com.whitecloak.cleancomponents.mvp.MvpPresenter;
import com.whitecloak.cleancomponents.mvp.MvpView;
import com.whitecloak.perahub.R;
import com.whitecloak.perahub.commons.model.RegistrationParam;
import com.whitecloak.perahub.registration.RegistrationActivity;
import com.whitecloak.perahub.registration.RegistrationActivityPresenter;
import com.whitecloak.perahub.registration.RegistrationActivityView;

public abstract class BaseRegistrationFragment<V extends MvpView, P extends MvpPresenter<V>> extends BaseMvpFragment<V, P> {

    protected RegistrationActivityView registrationActivityView;

    @Override
    protected void setupViews() {
        registrationActivityView = (RegistrationActivity)getActivity();
        ((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(
                InputMethodManager.SHOW_FORCED,
                InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    protected void replaceFragmentAddBackStack(Fragment fragment, String tag) {
        registrationActivityView.scrollViewScrollTop();
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.flFragmentContainer, fragment, tag).
                addToBackStack(tag);
        fragmentTransaction.commit();
    }



}
