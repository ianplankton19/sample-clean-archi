package com.whitecloak.perahub.commons.controller;

import android.content.Context;

import com.airbnb.epoxy.CarouselModel_;
import com.airbnb.epoxy.EpoxyController;
import com.whitecloak.perahub.commons.model.Dashboard;
import com.whitecloak.perahub.commons.view.DashboardViewHolder_;

import java.util.ArrayList;

public class DashboardItemController extends EpoxyController {

    private Context context;
    private ArrayList<Dashboard> dashboardArrayList = new ArrayList<>();

    public DashboardItemController(Context context) {
        this.context = context;
    }

    @Override
    protected void buildModels() {
        ArrayList<DashboardViewHolder_> dashboardViewHolders = new ArrayList<>();

        if (dashboardArrayList != null) {
            for (int i = 0; dashboardArrayList.size() > i; i++) {
                dashboardViewHolders.add(new DashboardViewHolder_()
                        .id(i)
                        .context(this.context)
                        .resource(dashboardArrayList.get(i).getImgDrawable())
                        .text(dashboardArrayList.get(i).getTitle()));
            }
        }

        new CarouselModel_()
                .id("carousel")
                .models(dashboardViewHolders)
                .addTo(this);
    }

    public void updateList(ArrayList<Dashboard> collections) {
        this.dashboardArrayList = collections;
        requestModelBuild();
    }
}
