package com.whitecloak.perahub.commons.model;

import android.graphics.drawable.Drawable;

public class Dashboard {

    public Dashboard(String imgDrawable, String title) {
        this.imgDrawable = imgDrawable;
        this.title = title;
    }

    private String imgDrawable;

    private String title;

    public String getImgDrawable() {
        return imgDrawable;
    }

    public void setImgDrawable(String imgDrawable) {
        this.imgDrawable = imgDrawable;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
