package com.whitecloak.perahub.commons.injection;

import com.whitecloak.perahub.login.LandingActivity;
import com.whitecloak.perahub.login.LoginActivity;
import com.whitecloak.perahub.login.LoginUserActivity;
import com.whitecloak.perahub.otp.OtpActivity;
import com.whitecloak.perahub.registration.PhoneInputActivity;
import com.whitecloak.perahub.registration.RegistrationActivity;
import com.whitecloak.perahub.registration.RegistrationOneFragment;
import com.whitecloak.perahub.registration.RegistrationThreeFragment;
import com.whitecloak.perahub.registration.RegistrationTwoFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;


/**
 * Created by mertsimsek on 25/05/2017.
 */
@Module
public abstract class ActivityBuilder {

    @ContributesAndroidInjector
    abstract LoginActivity bindLoginActivity();

    @ContributesAndroidInjector
    abstract LoginUserActivity bindLoginUserActivity();

    @ContributesAndroidInjector
    abstract OtpActivity bindOtpActivity();

    @ContributesAndroidInjector
    abstract PhoneInputActivity bindPhoneInputActivity();

    @ContributesAndroidInjector
    abstract LandingActivity bindHomeActivity();

    @ContributesAndroidInjector
    abstract RegistrationActivity bindRegistrationActivity();

    @ContributesAndroidInjector
    abstract RegistrationOneFragment bindsRegistrationOneFragment();

    @ContributesAndroidInjector
    abstract RegistrationTwoFragment bindsRegistrationTwoFragment();

    @ContributesAndroidInjector
    abstract RegistrationThreeFragment bindsRegistrationThreeFragment();
}
