package com.whitecloak.perahub.commons.injection;

import android.app.Application;

import com.whitecloak.perahub.PerahubApplication;
import com.whitecloak.perahub.commons.injection.modules.AppModule;
import com.whitecloak.perahub.commons.injection.modules.RepositoryModule;
import com.whitecloak.perahub.data.injection.modules.NetModule;
import com.whitecloak.perahub.data.injection.modules.PreferencesModule;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;
import dagger.android.support.AndroidSupportInjectionModule;
import io.reactivex.Scheduler;

@Singleton
@Component(modules = {
        AndroidSupportInjectionModule.class,
        AppModule.class,
        NetModule.class, PreferencesModule.class,
        RepositoryModule.class,
        ActivityBuilder.class
})

public interface AppComponent extends AndroidInjector<DaggerApplication> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);

        @BindsInstance
        Builder baseUrl(@Named("baseUrl") String baseUrl);

        @BindsInstance
        Builder packageName(@Named("packageName") String packageName);

        @BindsInstance
        Builder executionThread(@Named("execution") Scheduler scheduler);

        @BindsInstance
        Builder postExecutionThread(@Named("post_execution") Scheduler scheduler);

        AppComponent build();
    }

    void inject(PerahubApplication app);

}