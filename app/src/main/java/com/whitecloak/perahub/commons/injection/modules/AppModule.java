package com.whitecloak.perahub.commons.injection.modules;

import android.app.Application;
import android.content.Context;

import dagger.Binds;
import dagger.Module;

/**
 * Created by ian.blanco on 10/13/2017.
 */
@Module
public abstract class AppModule {

    @Binds
    abstract Context provideContext(Application application);
}