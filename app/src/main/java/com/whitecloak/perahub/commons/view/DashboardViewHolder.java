package com.whitecloak.perahub.commons.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airbnb.epoxy.EpoxyAttribute;
import com.airbnb.epoxy.EpoxyHolder;
import com.airbnb.epoxy.EpoxyModelClass;
import com.airbnb.epoxy.EpoxyModelWithHolder;
import com.bumptech.glide.Glide;
import com.whitecloak.perahub.R;

import butterknife.BindView;
import butterknife.ButterKnife;

@EpoxyModelClass(layout = R.layout.compound_dashboard_button)
public abstract class DashboardViewHolder extends EpoxyModelWithHolder<DashboardViewHolder.DashboardHolder> {

    @EpoxyAttribute
    String text;

    @EpoxyAttribute
    Context context;

    @EpoxyAttribute
    String resource;


    @Override
    public void bind(@NonNull DashboardHolder holder) {

        holder.tvTitle.setText(text);

        int resourceId = context.getResources().getIdentifier((String) resource,
                "drawable", context.getPackageName());
        Glide.with(context).
                load(resourceId).
                into(holder.ivOne);

    }

    static class DashboardHolder extends EpoxyHolder {

        @BindView(R.id.ivOne)
        ImageView ivOne;

        @BindView(R.id.tvTitle)
        TextView tvTitle;

        @BindView(R.id.rlParent)
        RelativeLayout rlParent;


        @Override
        protected void bindView(View itemView) {
            ButterKnife.bind(this, itemView);
        }
    }
}
