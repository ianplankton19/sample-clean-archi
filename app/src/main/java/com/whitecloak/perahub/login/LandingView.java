package com.whitecloak.perahub.login;

import com.whitecloak.perahub.commons.base.BaseView;
import com.whitecloak.perahub.commons.model.Dashboard;

import java.util.ArrayList;

public interface LandingView extends BaseView{

    void showMoreOptions(ArrayList<Dashboard> moreOptions);

    void navigateToLoginScreen();

    void navigateToLoginScreenWithPreferences();
}
