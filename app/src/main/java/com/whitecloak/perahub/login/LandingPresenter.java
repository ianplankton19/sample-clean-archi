package com.whitecloak.perahub.login;


import com.whitecloak.perahub.commons.base.BaseMvpPresenter;
import com.whitecloak.perahub.commons.model.Dashboard;
import com.whitecloak.perahub.domain.interactors.user.GetLoggedInUserUseCase;

import java.util.ArrayList;

import javax.inject.Inject;

public class LandingPresenter extends BaseMvpPresenter<LandingView> {

    private final GetLoggedInUserUseCase getLoggedInUserUseCase;

    @Inject
    public LandingPresenter(GetLoggedInUserUseCase getLoggedInUserUseCase) {
        this.getLoggedInUserUseCase = getLoggedInUserUseCase;
    }

    public void loadMoreOptions() {
        ArrayList<Dashboard> moreOptions = new ArrayList<>();

        moreOptions.add(new Dashboard("ic_pera_protect", "Services"));
        moreOptions.add(new Dashboard("ic_pera_protect", "Promos"));
        moreOptions.add(new Dashboard("ic_pera_protect", "Rewards"));
        moreOptions.add(new Dashboard("ic_pera_protect", "Branches"));
        moreOptions.add(new Dashboard("ic_pera_protect", "Branches"));
        moreOptions.add(new Dashboard("ic_pera_protect", "Branches"));
        moreOptions.add(new Dashboard("ic_pera_protect", "Branches"));
        moreOptions.add(new Dashboard("ic_pera_protect", "Branches"));
        moreOptions.add(new Dashboard("ic_pera_protect", "Branches"));

        getView().showMoreOptions(moreOptions);
    }

    public void checkLogin() {
        getLoggedInUserUseCase.execute()
                .subscribe(user -> getView().navigateToLoginScreenWithPreferences(),
                        throwable -> getView().navigateToLoginScreen());
    }
}
