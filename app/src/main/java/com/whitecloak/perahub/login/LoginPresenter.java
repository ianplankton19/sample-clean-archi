package com.whitecloak.perahub.login;

import com.whitecloak.perahub.commons.base.BaseMvpPresenter;
import com.whitecloak.perahub.commons.model.Dashboard;
import com.whitecloak.perahub.domain.interactors.user.LoginUseCase;

import java.util.ArrayList;

import javax.inject.Inject;

public class LoginPresenter extends BaseMvpPresenter<LoginView> {

    private final LoginUseCase loginUseCase;

    private String username;
    private String password;

    @Inject
    LoginPresenter(LoginUseCase loginUseCase) {
        this.loginUseCase = loginUseCase;
    }

    // sa tingin ko dapat sa attachview na to
    // kaso sa library natin ng clean components
    // nauuna iattach ung view sa presenter kahit ndi pa ready ung view
    public void setupInitialState() {
        getView().disableLogin();
    }

    public void setUsername(String username) {
        this.username = username;
        if (validateUsername()) {
            validateAll();
        }
    }

    public void setPassword(String password) {
        this.password = password;
        if (validatePassword()) {
            validateAll();
        }
    }

    private boolean validateUsername() {
        if (username == null || username.isEmpty()) {
            getView().showUsernameFieldError("Username is required");
            getView().disableLogin();
            return false;
        } else {
            // additional validations
//             example: don't allow skusta_clee to login
            if (username.equals("skusta_clee")) {
                getView().showUsernameFieldError("Shanti dope only");
                getView().disableLogin();
                return false;
            }
        }
        getView().clearUsernameFieldError();
        return true;
    }

    private boolean validatePassword() {
        if (password == null || password.isEmpty()) {
            getView().showPasswordFieldError("Password is required");
            getView().disableLogin();
            return false;
        }
        getView().clearPasswordFieldError();
        return true;
    }

    private void validateAll() {
        // check required fields
        if (username == null || password == null) {
            getView().disableLogin();
            return;
        }

        // additional validations
        // that are dependent to other fields

        getView().enableLogin();
    }

    public void login() {
        getView().showLoginProgress();
        loginUseCase.execute(username, password)
                .subscribe(mobileNumber -> {
                    getView().hideLoginProgress();
                    getView().navigateToOtpScreen(username, password, mobileNumber);
                }, throwable -> {
                    getView().hideLoginProgress();
                    getView().showLoginError(throwable.getLocalizedMessage());
                });
    }


    public void loadData() {
        ArrayList<Dashboard> dashboardArrayList = new ArrayList<>();
            dashboardArrayList.add(new Dashboard("ic_pera_protect", "Sample1"));
            dashboardArrayList.add(new Dashboard("ic_pera_protect", "Sample2"));
            dashboardArrayList.add(new Dashboard("ic_pera_protect", "Sample3"));
            dashboardArrayList.add(new Dashboard("ic_pera_protect", "Sample4"));
            dashboardArrayList.add(new Dashboard("ic_pera_protect", "Sample5"));
            dashboardArrayList.add(new Dashboard("ic_pera_protect", "Sample6"));
            dashboardArrayList.add(new Dashboard("ic_pera_protect", "Sample7"));
            dashboardArrayList.add(new Dashboard("ic_pera_protect", "Sample8"));
            dashboardArrayList.add(new Dashboard("ic_pera_protect", "Sample9"));
            dashboardArrayList.add(new Dashboard("ic_calendar_logo", "Sample10"));

            getView().displayDashboard(dashboardArrayList);

    }

}
