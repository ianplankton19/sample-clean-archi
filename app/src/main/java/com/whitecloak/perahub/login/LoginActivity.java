package com.whitecloak.perahub.login;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.whitecloak.perahub.R;
import com.whitecloak.perahub.commons.base.BaseMvpActivity;
import com.whitecloak.perahub.commons.model.Dashboard;
import com.whitecloak.perahub.otp.OtpActivity;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class LoginActivity extends BaseMvpActivity<LoginView, LoginPresenter> implements LoginView {


    public static Intent getIntent(@NonNull Context context) {
        return new Intent(context, LoginActivity.class);
    }

    private MaterialDialog materialDialog;

    @Inject
    LoginPresenter loginPresenter;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.etUsername)
    EditText etUsername;

    @BindView(R.id.etPassword)
    EditText etPassword;

    @BindView(R.id.btnLogin)
    Button btnLogin;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.vToolbarUnderline)
    View vToolbarUnderline;

    @OnTextChanged(value = R.id.etUsername,
            callback = OnTextChanged.Callback.TEXT_CHANGED)
    void usernameTextChanged(Editable editable) {
        loginPresenter.setUsername(String.valueOf(editable));
    }

    @OnTextChanged(value = R.id.etPassword,
            callback = OnTextChanged.Callback.TEXT_CHANGED)
    void passwordextChanged(Editable editable) {
        loginPresenter.setPassword(String.valueOf(editable));
    }


    @Override
    protected int getLayoutRes() {
        return R.layout.activity_login;
    }

    @NonNull
    @Override
    protected LoginView getMvpView() {
        return this;
    }

    @NonNull
    @Override
    protected LoginPresenter getMvpPresenter() {
        return loginPresenter;
    }

    @Override
    protected void setupDependencies() {

    }

    @Override
    protected void setupViews() {
        initToolbar();
        loginPresenter.setupInitialState();
    }

    @Override
    protected void loadContent() {

    }

    @OnClick(R.id.btnLogin)
    public void onViewClicked() {
        loginPresenter.login();

    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        tvTitle.setText("Login");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(R.drawable.ic_action_back);
        toolbar.setNavigationOnClickListener(v -> {
            finish();
        });
    }

    @Override
    public void showUsernameFieldError(String message) {
        etUsername.setError(message);
    }

    @Override
    public void clearUsernameFieldError() {
        etUsername.setError(null);
    }

    @Override
    public void showPasswordFieldError(String message) {
        etPassword.setError(message);
    }

    @Override
    public void clearPasswordFieldError() {
        etPassword.setError(null);
    }

    @Override
    public void disableLogin() {
        btnLogin.setEnabled(false);
    }

    @Override
    public void enableLogin() {
        btnLogin.setEnabled(true);
    }

    @Override
    public void showLoginProgress() {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                .title("Verify Login")
                .content("please wait..")
                .progress(true, 0);
        materialDialog = builder.build();
        materialDialog.show();
    }

    @Override
    public void hideLoginProgress() {
        materialDialog.dismiss();
    }

    @Override
    public void showLoginError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void navigateToOtpScreen(String username, String password, String mobileNumber) {
        startActivity(OtpActivity.getIntent(this, username, password,
                mobileNumber, "Login"));
        finish();
    }

    @Override
    public void displayDashboard(ArrayList<Dashboard> dashboardArrayList) {
//        this login function is for loginUserActivity view
    }


}

