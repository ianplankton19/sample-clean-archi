package com.whitecloak.perahub.login;

import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;

import com.airbnb.epoxy.EpoxyRecyclerView;
import com.whitecloak.perahub.R;
import com.whitecloak.perahub.commons.base.BaseMvpActivity;
import com.whitecloak.perahub.commons.controller.DashboardItemController;
import com.whitecloak.perahub.commons.model.Dashboard;
import com.whitecloak.perahub.registration.PhoneInputActivity;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class LandingActivity extends BaseMvpActivity<LandingView, LandingPresenter> implements LandingView {

    @BindView(R.id.recycler_view_more)
    EpoxyRecyclerView recyclerView;

    @Inject
    LandingPresenter presenter;

    DashboardItemController controller;

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_landing;
    }

    @Override
    protected LandingView getMvpView() {
        return this;
    }

    @NonNull
    @Override
    protected LandingPresenter getMvpPresenter() {
        return presenter;
    }

    @Override
    protected void setupDependencies() {
    }

    @Override
    protected void setupViews() {
        controller = new DashboardItemController(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setController(controller);
    }

    @Override
    protected void loadContent() {
        presenter.loadMoreOptions();
    }

    @OnClick(R.id.button_login)
    public void onClickLogin() {
        presenter.checkLogin();
    }

    @OnClick(R.id.button_register)
    public void onClickRegister() {
        startActivity(PhoneInputActivity.getIntent(this));
    }

    /* LandingView */

    @Override
    public void showMoreOptions(ArrayList<Dashboard> data) {
        controller.updateList(data);
    }

    @Override
    public void navigateToLoginScreen() {
        startActivity(LoginActivity.getIntent(this));
    }

    @Override
    public void navigateToLoginScreenWithPreferences() {
        startActivity(LoginUserActivity.getIntent(this));
    }
}
