package com.whitecloak.perahub.login;

import com.whitecloak.perahub.commons.base.BaseView;
import com.whitecloak.perahub.commons.model.Dashboard;

import java.util.ArrayList;

public interface LoginView extends BaseView {

    void showUsernameFieldError(String message);
    void clearUsernameFieldError();

    void showPasswordFieldError(String message);
    void clearPasswordFieldError();

    void disableLogin();
    void enableLogin();

    void showLoginProgress();
    void hideLoginProgress();
    void showLoginError(String message);

    void navigateToOtpScreen(String username, String password, String mobileNumber);

//    for loginuseractivity only
    void displayDashboard(ArrayList<Dashboard> dashboardArrayList);

}
