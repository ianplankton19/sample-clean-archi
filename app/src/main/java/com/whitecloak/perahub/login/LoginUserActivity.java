package com.whitecloak.perahub.login;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.airbnb.epoxy.EpoxyRecyclerView;
import com.whitecloak.perahub.R;
import com.whitecloak.perahub.commons.base.BaseMvpActivity;
import com.whitecloak.perahub.commons.controller.DashboardItemController;
import com.whitecloak.perahub.commons.model.Dashboard;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class LoginUserActivity extends BaseMvpActivity<LoginView, LoginPresenter>
        implements LoginView {

    public static Intent getIntent(Context context) {
        return new Intent(context, LoginUserActivity.class);
    }

    @Inject
    LoginPresenter loginPresenter;

    @BindView(R.id.civUserImg)
    CircleImageView civUserImg;

    @BindView(R.id.tvFullname)
    TextView tvFullname;

    @BindView(R.id.tvUsername)
    TextView tvUsername;

    @BindView(R.id.etPassword)
    EditText etPassword;

    @BindView(R.id.btnLogin)
    Button btnLogin;

    @BindView(R.id.ervDashboard)
    EpoxyRecyclerView ervDashboard;

    DashboardItemController dashboardItemController;

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_login_user;
    }

    @NonNull
    @Override
    protected LoginView getMvpView() {
        return this;
    }

    @NonNull
    @Override
    protected LoginPresenter getMvpPresenter() {
        return loginPresenter;
    }

    @Override
    protected void setupDependencies() {

    }

    @Override
    protected void setupViews() {

    }

    @Override
    protected void loadContent() {
        dashboardItemController = new DashboardItemController(this);
        ervDashboard.setLayoutManager(new LinearLayoutManager(this));
        ervDashboard.setController(dashboardItemController);
        loginPresenter.loadData();
    }

    @Override
    public void showUsernameFieldError(String message) {

    }

    @Override
    public void clearUsernameFieldError() {

    }

    @Override
    public void showPasswordFieldError(String message) {

    }

    @Override
    public void clearPasswordFieldError() {

    }

    @Override
    public void disableLogin() {

    }

    @Override
    public void enableLogin() {

    }

    @Override
    public void showLoginProgress() {

    }

    @Override
    public void hideLoginProgress() {

    }

    @Override
    public void showLoginError(String message) {

    }

    @Override
    public void navigateToOtpScreen(String username, String password, String mobileNumber) {

    }


    @Override
    public void displayDashboard(ArrayList<Dashboard> dashboardArrayList) {
        dashboardItemController.updateList(dashboardArrayList);

    }
}
