package com.whitecloak.domain.helpers;

import com.whitecloak.domain.injection.DaggerTestAppComponent;
import com.whitecloak.domain.injection.TestAppComponent;

import io.reactivex.schedulers.Schedulers;

/**
 * Created by johneris on 1/1/17.
 */

public class DaggerHelper {

    public static TestAppComponent getTestAppComponent() {
        return DaggerTestAppComponent.builder()
                .executionThread(Schedulers.io())
                .postExecutionThread(Schedulers.io())
                .build();
    }

}
