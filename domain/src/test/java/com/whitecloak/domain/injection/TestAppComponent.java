package com.whitecloak.domain.injection;

import com.whitecloak.domain.interactors.user.GetLoggedInUserUseCaseTest;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import io.reactivex.Scheduler;

/**
 * Created by johneris on 3/7/18.
 */

@Singleton
@Component(modules = {
})
public interface TestAppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder executionThread(@Named("execution") Scheduler scheduler);

        @BindsInstance
        Builder postExecutionThread(@Named("post_execution") Scheduler scheduler);

        TestAppComponent build();
    }

    void inject(GetLoggedInUserUseCaseTest getLoggedInUserUseCaseTest);

}

