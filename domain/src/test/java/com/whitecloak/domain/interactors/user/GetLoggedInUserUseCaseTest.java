package com.whitecloak.domain.interactors.user;

import com.whitecloak.domain.helpers.DaggerHelper;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Created by johneris on 3/10/18.
 */

@RunWith(MockitoJUnitRunner.class)
public class GetLoggedInUserUseCaseTest {

    @Before
    public void setUp() {
        DaggerHelper.getTestAppComponent().inject(this);
    }

}