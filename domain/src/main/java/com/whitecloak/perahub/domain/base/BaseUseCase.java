package com.whitecloak.perahub.domain.base;

import javax.inject.Inject;

import io.reactivex.ObservableTransformer;
import io.reactivex.Scheduler;

/**
 * Created by johneris on 2/24/18.
 */

public class BaseUseCase {

    Scheduler executionThread;
    Scheduler postExecutionThread;

    @Inject
    public BaseUseCase(Scheduler executionThread,
                       Scheduler postExecutionThread) {
        this.executionThread = executionThread;
        this.postExecutionThread = postExecutionThread;
    }

    protected <T> ObservableTransformer<T, T> applySchedulers() {
        return upstream -> upstream.subscribeOn(executionThread)
                .observeOn(postExecutionThread);
    }

}
