package com.whitecloak.perahub.domain.repositories

import com.whitecloak.perahub.domain.models.User
import com.whitecloak.perahub.domain.repositories.params.RegisterParam
import com.whitecloak.perahub.domain.repositories.params.RequestRegisterOtpParam
import com.whitecloak.perahub.domain.repositories.params.ValidateRegisterOtpParam
import com.whitecloak.perahub.domain.repositories.resp.RequestRegisterOtpResp
import com.whitecloak.perahub.domain.repositories.resp.ValidateRegisterOtpResp
import io.reactivex.Completable
import io.reactivex.Observable

/**
 * Created by johneris on 3/14/18.
 */

interface RegistrationRepository {
    fun getOtpToken(): Observable<String>
    fun getRegistrationToken(): Observable<String>
    fun register(param: RegisterParam): Observable<User>
    fun requestRegisterOtp(param: RequestRegisterOtpParam): Observable<RequestRegisterOtpResp>
    fun saveRegistrationToken(token: String): Completable
    fun saveOtpToken(token: String): Completable
    fun validateRegisterOtp(param: ValidateRegisterOtpParam): Observable<ValidateRegisterOtpResp>
}