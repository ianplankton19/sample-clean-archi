package com.whitecloak.perahub.domain.interactors.user

import com.whitecloak.perahub.domain.base.BaseUseCase
import com.whitecloak.perahub.domain.models.User
import com.whitecloak.perahub.domain.repositories.UserRepository
import com.whitecloak.perahub.domain.repositories.params.ValidateOtpParam
import io.reactivex.Observable
import io.reactivex.Scheduler
import javax.inject.Inject
import javax.inject.Named

/**
 * Created by johneris on 3/14/18.
 */

class ValidateLoginUseCase @Inject
constructor(@Named("execution") executionThread: Scheduler,
            @Named("post_execution") postExecutionThread: Scheduler,
            private val userRepository: UserRepository) : BaseUseCase(executionThread, postExecutionThread) {

    fun execute(otpCode: String): Observable<User> {
        return userRepository.getLoginOtpToken()
                .flatMap {
                    val param = ValidateOtpParam(otpCode, it)
                    userRepository.validateLogin(param)
                }.doOnNext {
                    userRepository.loggedInUser = Observable.just(it)
                }
    }


}
