package com.whitecloak.perahub.domain.repositories.params

import com.google.gson.annotations.SerializedName

/**
 * Created by johneris on 3/14/18.
 */

data class RequestRegisterOtpParam(
        @SerializedName("mobile_no") val mobileNumber: String
)