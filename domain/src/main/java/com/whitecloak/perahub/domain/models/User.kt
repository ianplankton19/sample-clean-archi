package com.whitecloak.perahub.domain.models

import com.google.gson.annotations.SerializedName

/**
 * Created by johneris on 2/28/18.
 */

data class User(
        @SerializedName("username") val username: String,
        @SerializedName("mobile") val mobile: String,
        @SerializedName("email") val email: String,
        @SerializedName("phone") val phone: String,
        @SerializedName("surname") val surname: String,
        @SerializedName("first_name") val firstName: String,
        @SerializedName("middle_name") val middleName: String,
        @SerializedName("password") val password: String,
        @SerializedName("birthdate") val birthdate: String,
        @SerializedName("nationality") val nationality: String,
        @SerializedName("occupation") val occupation: String,
        @SerializedName("name_of_employer") val nameOfEmployer: String,
        @SerializedName("secret_question_1") val secretQuestion1: String,
        @SerializedName("answer_1") val answer1: String,
        @SerializedName("secret_question_2") val secretQuestion2: String,
        @SerializedName("answer_2") val answer2: String,
        @SerializedName("secret_question_3") val secretQuestion3: String,
        @SerializedName("answer_3") val answer3: String,
        @SerializedName("valid_identification") val validIdentification: String,
        @SerializedName("id_image") val idImage: String,
        @SerializedName("wu_card_no") val wuCardNo: String,
        @SerializedName("debit_card_no") val debitCardNo: String,
        @SerializedName("loyalty_card_no") val loyaltyCardNo: String,
        @SerializedName("valid_id_number") val validIdNumber: Long,
        @SerializedName("id_country_issue") val idCountryIssue: String,
        @SerializedName("id_issue_data") val idIssueData: String,
        @SerializedName("gender") val gender: String,
        @SerializedName("is_walkin") val isWalkin: String,
        @SerializedName("customer_id") val customerId: String,
        @SerializedName("id_type") val idType: String,
        @SerializedName("id_expiration_date") val idExpirationDate: String,
        @SerializedName("country_of_birth") val countryOfBirth: String,
        @SerializedName("tin") val tin: String,
        @SerializedName("sss") val sss: Int,
        @SerializedName("present_address") val presentAddress: Address,
        @SerializedName("permanent_address") val permanentAddress: Address
)