package com.whitecloak.perahub.domain.repositories.resp

import com.google.gson.annotations.SerializedName

/**
 * Created by johneris on 3/14/18.
 */

data class RequestRegisterOtpResp(
        @SerializedName("request_id") val requestId: String
)