package com.whitecloak.perahub.domain.interactors.registration

import com.whitecloak.perahub.domain.base.BaseUseCase
import com.whitecloak.perahub.domain.repositories.RegistrationRepository
import com.whitecloak.perahub.domain.repositories.params.ValidateRegisterOtpParam
import io.reactivex.Completable
import io.reactivex.Scheduler
import javax.inject.Inject
import javax.inject.Named

/**
 * Created by johneris on 3/14/18.
 */

class ValidateRegisterOtpUseCase @Inject
constructor(@Named("execution") executionThread: Scheduler,
            @Named("post_execution") postExecutionThread: Scheduler,
            private val registrationRepository: RegistrationRepository) : BaseUseCase(executionThread, postExecutionThread) {

    fun execute(otpCode: String): Completable {
        return registrationRepository.getOtpToken()
                .flatMap {
                    val param = ValidateRegisterOtpParam(otpCode, it )
                    registrationRepository.validateRegisterOtp(param)
                }.flatMapCompletable {
                    registrationRepository.saveRegistrationToken(it.requestId)
                }
    }

}