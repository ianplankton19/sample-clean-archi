package com.whitecloak.perahub.domain.form.interactor

import com.whitecloak.perahub.domain.form.model.RegistrationForm
import com.whitecloak.perahub.domain.form.repository.FormRepository
import io.reactivex.Completable
import javax.inject.Inject

/**
 * Created by ralph on 14/03/2018.
 */

class ClearRegistrationForm @Inject constructor(private val formRepository: FormRepository) {

    fun execute() : Completable {
        return Completable.fromAction {
            formRepository.setRegistrationForm(null)
        }
    }
}