package com.whitecloak.perahub.domain.form.model

import com.google.gson.annotations.SerializedName
import com.whitecloak.perahub.domain.models.Address

/**
 * Created by ralph on 13/03/2018.
 */

data class RegistrationForm(
        @SerializedName("phone_no")var phoneNo: String? = null,
        @SerializedName("username")var userName: String? = null,
        @SerializedName("password")var password: String? = null,
        @SerializedName("first_name")var firstName: String? = null,
        @SerializedName("middle_name")var middleName: String? = null,
        @SerializedName("last_name")var lastName: String? = null,
        @SerializedName("birthday")var birthday: String? = null,
        @SerializedName("address")var address: String? = null,
        @SerializedName("nationality")var nationality: String? = null,
        @SerializedName("email")var email: String? = null,
        @SerializedName("wu_card_no")var cardNo: String? = null,
        @SerializedName("perahub_prepaid") var perahubPrepaid: String? = null,
        @SerializedName("loyalty_card_no") var loyaltyCardNo: String? = null,
        @SerializedName("present_address")var presentAddress: Address? = null,
        @SerializedName("permanent_address")var permanentAddress: Address? = null
)