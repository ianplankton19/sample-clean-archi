package com.whitecloak.perahub.domain.interactors.registration

import com.whitecloak.perahub.domain.base.BaseUseCase
import com.whitecloak.perahub.domain.form.model.RegistrationForm
import com.whitecloak.perahub.domain.models.User
import com.whitecloak.perahub.domain.repositories.RegistrationRepository
import com.whitecloak.perahub.domain.repositories.params.RegisterParam
import io.reactivex.Observable
import io.reactivex.Scheduler
import javax.inject.Inject
import javax.inject.Named

/**
 * Created by johneris on 3/14/18.
 */

class RegisterUseCase @Inject
constructor(@Named("execution") executionThread: Scheduler,
            @Named("post_execution") postExecutionThread: Scheduler,
            private val registrationRepository: RegistrationRepository) : BaseUseCase(executionThread, postExecutionThread) {

    fun execute(form: RegistrationForm): Observable<User> {


        return registrationRepository.getRegistrationToken()
                .flatMap {
                    val param = RegisterParam(it, form)
                    registrationRepository.register(param)
                }.compose(applySchedulers())
    }

}