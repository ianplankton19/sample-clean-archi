package com.whitecloak.perahub.domain.form.repository

import com.whitecloak.perahub.domain.form.model.RegistrationForm

/**
 * Created by ralph on 13/03/2018.
 */

interface FormRepository {

    fun setRegistrationForm(form: RegistrationForm?)

    val getRegistrationForm: RegistrationForm?
}