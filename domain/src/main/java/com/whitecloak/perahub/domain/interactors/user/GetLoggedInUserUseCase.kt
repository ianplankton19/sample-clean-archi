package com.whitecloak.perahub.domain.interactors.user

import com.whitecloak.perahub.domain.base.BaseUseCase
import com.whitecloak.perahub.domain.models.User
import com.whitecloak.perahub.domain.repositories.UserRepository
import io.reactivex.Observable
import io.reactivex.Scheduler
import javax.inject.Inject
import javax.inject.Named

/**
 * Created by johneris on 2/27/18.
 */

class GetLoggedInUserUseCase @Inject
constructor(@Named("execution") executionThread: Scheduler,
            @Named("post_execution") postExecutionThread: Scheduler,
            private val userRepository: UserRepository) : BaseUseCase(executionThread, postExecutionThread) {

    fun execute(): Observable<User> {
        return userRepository.loggedInUser
                .compose(applySchedulers())
    }

}
