package com.whitecloak.perahub.domain.form.interactor

import com.whitecloak.perahub.domain.form.model.RegistrationForm
import com.whitecloak.perahub.domain.form.repository.FormRepository
import com.whitecloak.perahub.domain.models.Address
import io.reactivex.Completable
import javax.inject.Inject

/**
 * Created by ralph on 14/03/2018.
 */

class UpdateRegistrationForm @Inject constructor(private val formRepository: FormRepository) {

    fun credentials(userName: String, password: String): Completable {
        return Completable.fromAction {
            val form: RegistrationForm = getForm()
            val updatedForm = form.copy(userName = userName, password = password)
            formRepository.setRegistrationForm(updatedForm)
        }
    }

    fun info(firstName: String, middleName: String, lastName: String,
             birthday: String, address: String, nationality: String, email: String): Completable {
        return Completable.fromAction {
            val form: RegistrationForm = getForm()
            val updatedForm = form.copy(
                    firstName = firstName,
                    middleName = middleName,
                    lastName = lastName,
                    birthday = birthday,
                    address = address,
                    nationality = nationality,
                    email = email,
                    perahubPrepaid =  "",
                    loyaltyCardNo =  "",
                    permanentAddress = Address("test", "test", "test",
                            "test", "test", "test", "1234"),
                    presentAddress =  Address("test", "test", "test",
                            "test", "test", "test", "1234")


            )
            formRepository.setRegistrationForm(updatedForm)
        }
    }

    fun cardDetails(cardNo: String): Completable {
        return Completable.fromAction {
            val form: RegistrationForm = getForm()
            val updatedForm = form.copy(cardNo = cardNo)
            formRepository.setRegistrationForm(updatedForm)
        }

    }

    fun getForm(): RegistrationForm {
        return formRepository.getRegistrationForm ?: return RegistrationForm()
    }
}