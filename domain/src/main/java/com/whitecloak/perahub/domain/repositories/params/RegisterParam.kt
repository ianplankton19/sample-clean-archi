package com.whitecloak.perahub.domain.repositories.params

import com.google.gson.annotations.SerializedName
import com.whitecloak.perahub.domain.form.model.RegistrationForm

class RegisterParam(
        @SerializedName("request_id") val requestId: String,
        @SerializedName("user") val user: RegistrationForm
)