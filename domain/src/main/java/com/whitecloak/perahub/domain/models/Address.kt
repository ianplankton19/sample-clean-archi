package com.whitecloak.perahub.domain.models

import com.google.gson.annotations.SerializedName

/**
 * Created by johneris on 3/13/18.
 */

data class Address(
        @SerializedName("address") val address: String,
        @SerializedName("city") val city: String,
        @SerializedName("state") val state: String,
        @SerializedName("province") val province: String,
        @SerializedName("region") val region: String,
        @SerializedName("country") val country: String,
        @SerializedName("postal_code") val postalCode: String
)