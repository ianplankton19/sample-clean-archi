package com.whitecloak.perahub.domain.repositories.resp

import com.google.gson.annotations.SerializedName

/**
 * Created by johneris on 3/14/18.
 */

data class OtpResp(
        @SerializedName("request_id") val requestId: String,
        @SerializedName("mobile_no") val mobileNumber: String
)