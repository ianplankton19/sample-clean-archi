package com.whitecloak.perahub.domain.interactors.user

import com.whitecloak.perahub.domain.base.BaseUseCase
import com.whitecloak.perahub.domain.repositories.UserRepository
import com.whitecloak.perahub.domain.repositories.params.LoginParam
import io.reactivex.Observable
import io.reactivex.Scheduler
import javax.inject.Inject
import javax.inject.Named

/**
 * Created by johneris on 2/27/18.
 */

class LoginUseCase @Inject
constructor(@Named("execution") executionThread: Scheduler,
            @Named("post_execution") postExecutionThread: Scheduler,
            private val userRepository: UserRepository) : BaseUseCase(executionThread, postExecutionThread) {

    fun execute(username: String, password: String): Observable<String> {
        var param = LoginParam(username, password)
        return userRepository.login(param)
                .compose(applySchedulers())
                .flatMapSingle {
                    userRepository.saveLoginOtpToken(it.requestId)
                            .toSingle { it.mobileNumber }
                }.flatMap {
                    Observable.just(it)
                }
    }

}
