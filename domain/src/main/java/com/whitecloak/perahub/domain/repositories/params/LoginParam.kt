package com.whitecloak.perahub.domain.repositories.params

import com.google.gson.annotations.SerializedName

/**
 * Created by johneris on 2/28/18.
 */

data class LoginParam(
        @SerializedName("username") val username: String,
        @SerializedName("password") var password: String
)