package com.whitecloak.perahub.domain.repositories

import com.whitecloak.perahub.domain.models.User
import com.whitecloak.perahub.domain.repositories.params.LoginParam
import com.whitecloak.perahub.domain.repositories.params.ValidateOtpParam
import com.whitecloak.perahub.domain.repositories.resp.OtpResp
import io.reactivex.Completable
import io.reactivex.Observable

/**
 * Created by johneris on 2/27/18.
 */

interface UserRepository {
    fun getLoginOtpToken(): Observable<String>
    var loggedInUser: Observable<User>
    fun saveLoginData(user: User)
    fun login(param: LoginParam): Observable<OtpResp>
    fun saveLoginOtpToken(token: String): Completable
    fun validateLogin(param: ValidateOtpParam): Observable<User>
}
