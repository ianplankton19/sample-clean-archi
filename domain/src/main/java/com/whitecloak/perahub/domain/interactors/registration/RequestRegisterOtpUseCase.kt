package com.whitecloak.perahub.domain.interactors.registration

import com.whitecloak.perahub.domain.base.BaseUseCase
import com.whitecloak.perahub.domain.repositories.RegistrationRepository
import com.whitecloak.perahub.domain.repositories.params.RequestRegisterOtpParam
import io.reactivex.Completable
import io.reactivex.Scheduler
import javax.inject.Inject
import javax.inject.Named

/**
 * Created by johneris on 3/14/18.
 */

class RequestRegisterOtpUseCase @Inject
constructor(@Named("execution") executionThread: Scheduler,
            @Named("post_execution") postExecutionThread: Scheduler,
            private val registrationRepository: RegistrationRepository) : BaseUseCase(executionThread, postExecutionThread) {

    fun execute(mobileNumber: String): Completable {
        var param = RequestRegisterOtpParam(mobileNumber)
        return registrationRepository.requestRegisterOtp(param)
                .compose(applySchedulers())
                .flatMapCompletable {
                    registrationRepository.saveOtpToken(it.requestId)
                }
    }

}