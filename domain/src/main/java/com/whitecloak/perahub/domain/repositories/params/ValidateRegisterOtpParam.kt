package com.whitecloak.perahub.domain.repositories.params

import com.google.gson.annotations.SerializedName

/**
 * Created by johneris on 3/14/18.
 */

data class ValidateRegisterOtpParam(
        @SerializedName("code") val otpCode: String,
        @SerializedName("request_id") val requestId: String
)