package com.whitecloak.perahub.domain.repositories.params

import com.google.gson.annotations.SerializedName

/**
 * Created by johneris on 3/14/18.
 */

data class ValidateOtpParam(
        @SerializedName("code") val otpCode: String,
        @SerializedName("request_id") var requestId: String
)